'use strict'

require('dotenv').config();

const Client = require('../lib/ccure/Client.js');
const Service = require('../lib/ccure/Service.js');

const client = new Client();
const service = new Service(client);

const [node, script, type, ...others] = process.argv;

console.log(`Finding schema for ${type}...`);

client.schema_get(type)
.then(result => { 
	console.log(result);
})
.catch(err => {
	console.log("ERROR!!!", err)
})
