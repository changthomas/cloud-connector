'use strict'

require('dotenv').config();

const Client = require('../lib/ccure/Client.js');
//const Service = require('../lib/ccure/Service.js');

const client = new Client();
//const service = new Service(client);

const [node, script, type, id, ...others] = process.argv;

console.log(`Finding ${type} with ID ${id}...`);

client.objects_get(type, id)
.then(result => { 
	console.log(result);
})
.catch(err => {
	console.log("ERROR!!!", err)
})
