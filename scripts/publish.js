'use strict';

require('dotenv').config()
const fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({apiVersion: '2006-03-01'});

//Get the filename from CLI args
const [node, script, filename, ...others] = process.argv;
console.log(`Publishing ${filename}`);

//Get just the basename
const basename = path.basename(filename);

let Bucket = process.env.AWS_S3_BUCKET;
let Key = path.join(process.env.AWS_S3_PATH || "", basename);
console.log(`To: s3://${Bucket}/${Key}`)

let file = fs.createReadStream(filename)

s3.upload(
	{
		Bucket, Key,
		ACL: "public-read",
		Body: file
	}, 
	function(err, data) {
		console.log(err, data);
	}
);

