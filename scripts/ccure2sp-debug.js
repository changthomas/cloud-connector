/*
ccure2sp-debug connects to RabbitMQ and fetches the next object in the
queue named ccure2sp-debug, or waits for the next message.

The ccure2sp-debug queue binds to the ccure exchange and uses the ccure2sp
routing key to receive duplicate messages being sent from SV3CC to Shortpath,
such that this operation does not interfere with data being sent to Shortpath.
*/

'use strict'

require('dotenv').config();
const amqp = require('amqplib');
const fs = require('fs');
const path = require('path');
const moment = require('moment');

const url = new URL(process.env.PROD_MQ_VHOST, "amqps://localhost:5672")
url.host = process.env.PROD_MQ_HOST
url.port = process.env.PROD_MQ_PORT
url.username = process.env.PROD_MQ_LOGIN
url.password = process.env.PROD_MQ_PASSWORD
url.search = 'frameMax=0x00'

//console.log(url)


class Ccure2SpDebugger {
	constructor(url) {
		this.url = url;
		this.count = 0;
	}

	connect() {
		console.log(this.url)
		return amqp.connect(this.url, {rejectUnauthorized: false})
		.then(conn => {
			console.log("Connected. Creating Channel...")
			this.conn = conn;
			return conn.createChannel()
		})
		.then(ch => {
			console.log("Channel open.")
			this.channel = ch;
			return Promise.all([
				ch.assertExchange('ccure', 'topic', {autoDelete: true, durable:true}), //
				//ch.assertQueue('ccure2sp_debug', {exclusive: true, durable: true})
				ch.checkQueue('ccure2sp_debug')
			]);
		})
		.then(([ex, q]) => {
			this.exchange = ex;
			this.queue = q;
			//Bind queue to exchange using routing key 'ccure2sp';
			return this.channel.bindQueue(q.queue, 'ccure', 'ccure2sp')
		})
	}

	start() {
		this.channel.consume(this.queue.queue, (msg) => {
			this.process(msg);
		})
		.then(tag => {
			console.log("Consuming with tag: ", tag);
		})
		console.log("Start consuming messages...")
	}

	process(msg) {
		let n = (''+this.count++).padStart(3,'0'); //increment message count

		//output headers to console
		console.log(`${n}: Got Message!`)
		
		let basename = `message-${n}-${moment().format('hhmmss.SSS')}`

		//save metadata and content to file
		Promise.all([
			writeFile(`${basename}.json`, JSON.stringify({fields: msg.fields, properties: msg.properties})),
			writeFile(`${basename}.bin`, msg.content)
		])
		.then(() => {
			this.channel.ack(msg);
		})
		.catch(err => {
			console.log("Error!", err);
		})
	}

}


function writeFile(name, content) {
	return new Promise((resolve, reject) => {
		fs.writeFile(name, content, (err) => {
			if (err) reject(err)
			else resolve()
		})
	})
}


const params = {
	protocol: 'amqps',
	hostname: process.env.PROD_MQ_HOST,
	port: process.env.PROD_MQ_PORT,
	username: process.env.PROD_MQ_LOGIN,
	password: process.env.PROD_MQ_PASSWORD,
	frameMax: 0,
	heartbeat: 0,
	vhost: process.env.PROD_MQ_VHOST
}

//console.log(params)

const d = new Ccure2SpDebugger(params);
d.connect()
.then(()=>{
	d.start()
})
.catch(err => {
	console.log("Error connecting!", err);
})
