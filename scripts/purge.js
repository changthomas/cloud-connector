'use strict'

require('dotenv').config();

const Personnel = require('../lib/ccure/Personnel.js');
const Group = require('../lib/ccure/Group.js');
const Credential = require('../lib/ccure/Credential.js');
const Visit = require('../lib/ccure/Visit.js');
const Clearance = require('../lib/ccure/Clearance.js');

const Client = require('../lib/ccure/Client.js');
const Service = require('../lib/ccure/Service.js');

const client = new Client();
const service = new Service(client);

//const [node, script, type, id, ...others] = process.argv;


(() => {
	console.log("PURGING CCURE OF ALL SV3-related DATA!\n");

	console.log("Purging ALL Personnel")
	return Promise.all([
		service.findAll(Personnel),
		service.findAll(Group, {GroupType: Personnel.TYPE})
	])
	.then(([personnel, groups]) => {
		return Promise.all(
			personnel.map(p => { return service.destroy(p) })
		)
		.then(() => {
			console.log("Purging ALL Personnel Groups")
			return Promise.all(groups.map(g => { return service.destroy(g) }))
		})
	})
	.then(() => {
		console.log("Purging ALL Credentials")
		return service.findAll(Credential) //Grabs ALL credentials
	})
	.then(credentials => {
		return Promise.all(credentials.map(c => { return service.destroy(c) }))
	})
	.then(() => {
		console.log("Purging all Visits");
		return service.findAll(Visit)
	})
	.then(visits => {
		return Promise.all(visits.map(v => { return service.destroy(v) }));
	})
	.then(() => {
		console.log("Purging all Clearances");
		return service.findAll(Clearance);
	})
	.then(clearances => {
		return Promise.all(clearances.map(c => { return service.destroy(c) }));
	})
	.then(() => {
		console.log("Purge complete.");
	})
	.catch(err => {
		console.log("Purge encountered an ERROR!", err);
	})
})();