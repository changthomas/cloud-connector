'use strict'

require('dotenv').config()

const random = require('../lib/util/random.js');
const moment = require('moment');
const _ = require('lodash');

// Models
const Credential = require('../lib/ccure/Credential.js');
const Partition = require('../lib/ccure/Partition.js');
const Personnel = require('../lib/ccure/Personnel.js');
const PersonnelType = require('../lib/ccure/PersonnelType.js');
const Visit = require('../lib/ccure/Visit.js');

const Client = require('../lib/ccure/Client.js');
const Service = require('../lib/ccure/Service.js');
const EmployeeHelper = require('../lib/ccure/util/EmployeeHelper.js');

const client = new Client();
const service = new Service(client);
const helper = new EmployeeHelper(service);


const [node, script, howmany, withcards, ...others] = process.argv;


service.login()
.then(() => {
	console.log(`Adding ${parseInt(howmany)} Employees with Credentials`);
	return helper.populate(parseInt(howmany))
})
.then(people => {
	console.log("Done.");
})
.catch(err => {
	//Probably an uncaught ESOCKETTIMEDOUT due to overloading IIS.
	//TODO: Investigate further. Are we reaching the server's socket connection limit (probably)
	//or is this a Node.js limit?  Try rate limiting?
	console.log("Error!", err);
});