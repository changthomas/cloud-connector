'use strict';


const fs = require('fs');

let filename = process.argv[2]; //'payload-20180313.json';

new Promise((resolve, reject) => {
	fs.readFile(filename, (err, data) => {
		if (err) reject(err);
		else resolve(data);
	});
})
.then((json) => {
	let data = JSON.parse(json);
	console.log("Customer code: %s", data.customer_code);
	console.log("Data type: %s", data.data_type);

	console.log("Data:");
	Object.keys(data.data).forEach(k => {
		console.log("%s: %s records", k, data.data[k].length)
	})

	console.log("Searching for %s %s", process.argv[3], process.argv[4])


	let matches = data.data.personnels.filter(p => {
		return (p.first_name.toLowerCase().indexOf(process.argv[3]) >= 0) &&
		(p.last_name.toLowerCase().indexOf(process.argv[4]) >= 0)
	})
	console.log(matches)

})
.catch(err => {
	console.log("ERROR", err)
})