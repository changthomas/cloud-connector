const moment = require('moment')

const Entity = require('./Entity')
const Client = require('../lib/Client')
const Credential = require('./Credential')
const CardholderGroup = require('./CardholderGroup')

class Visitor extends Entity {

  /*
    Class initializer

    @param {object} params - object attributes
      example object
      {
        logicalId: '', 
        firstName: '', 
        lastName: '',
        emailAddress: '', 
        departure: '2018-02-12T20:31:00.02-05:00'
      }
    @param {object} opts - additional options(currently not being used)
  */
  constructor(params, opts){
    super(params, opts)
    this.entityType = "Visitor"
  }

  /*
    Check in visitor

    @param {object} json - visitor object received from sv3 through amqp
      example object
      {
        first_name: '',
        last_name: '',
        card: 123456,
        active_date: '', //string in format "%m/%d/%Y 00:00:00"
        expiry_date: '', //string in format "%m/%d/%Y 00:00:00"
        email: 'visit.guest.email',
        guid: 'visit.guest.uuid',
        clearance_ids: [] //list of group ids to add to
      }
    @param {string} visitorGroupName - visitor group name
    @return {promise}
  */
  static checkIn(json, visitorGroupName){
    //2018-02-12T20:31:00.02-05:00
    let exp = moment(json.expiry_date).format('YYYY-MM-DDTHH:mm:ss')
    let visitorJson = {
      logicalId: `1${json.card}`,
      firstName: json.first_name,
      lastName: json.last_name,
      emailAddress: json.email,
      departure: exp
    }

    let credentialJson = {
      logicalId: json.card, 
      name: "VisitorCredential", 
      cardNumber: json.card
    }

    let p1 = CardholderGroup.findGroupByName(visitorGroupName)
    let p2 = (new Visitor(visitorJson)).forceCreate()
    let p3 = (new Credential(credentialJson)).forceCreate()

    let pf = Promise.all([p1, p2, p3]).then((r) => {
      let visitorGroup = r[0]
      let p4 = Credential.addCredentialToVisitor(credentialJson.logicalId, visitorJson.logicalId)
      let p5 = CardholderGroup.addCardholderToGroup("Visitor", visitorJson.logicalId, visitorGroup.guid)
      if(json.clearance_ids){
        json.clearance_ids.forEach((group_id) => {
          CardholderGroup.addCardholderToGroup("Visitor", visitorJson.logicalId, group_id)
        })
      }
    })
    return pf
  }
  
}

module.exports = Visitor
