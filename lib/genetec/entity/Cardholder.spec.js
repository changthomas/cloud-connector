const chai = require('chai')
const expect = chai.expect
const Cardholder = require('./Cardholder')
const fs = require('fs')
const path = require('path')
const nock = require('nock')
const xml2js = require('xml2js-parser').parseStringSync

process.env.NODE_ENV='test'

describe('Cardholder', () => {

  beforeEach(() => {
    let reqheaders = {}

    let cardHolderListResps = xml2js(`
      <rsp status="ok">
        <QueryResult>
          <HeaderRow>
            <Cell>
              <HeaderColumnName>Guid</HeaderColumnName>
              <Type>System.Guid</Type>
            </Cell>
          </HeaderRow>
          <Row>
            <Cell name="Guid">dcc50c515bdd433c94c22fd94254f8c7</Cell>
          </Row>
          <Row>
            <Cell name="Guid">e3af6ea9b0a6474f8465ecf0f4a8c9f6</Cell>
          </Row>
        </QueryResult>
      </rsp>`)

    nock('http://localhost:4590',{reqheaders: reqheaders})
      .get('/WebSdk/report/EntityConfiguration?q=EntityTypes@Cardholder,PageSize=10000,Page=1')
      .reply(200, cardHolderListResps)

  });
 
  describe('#getListGuids', () => {
    it('should return list of guids', () => {
      Cardholder.getListGuids().then((r) => {
        expect(r.length).to.equal(2)
      })
    })
  })

  
})





// // Import chai.

// // Import the classes
// // import { Cardholder } from '../entity/Cardholder'
// const Cardholder = require('../entity/Cardholder')

// //
// let cardholderJ1 = {
//   logicalId: "20001",
//   firstName: "TestF1",
//   lastName: "TestL1",
//   emailAddress: 'test1@test.com'
// }
// let cardholderJ2 = {
//   logicalId: "20002",
//   firstName: "TestF2",
//   lastName: "TestL2",
//   emailAddress: 'test2@test.com'
// }

// //
// let p1 = (new Cardholder(cardholderJ1)).forceCreate()
// let p2 = (new Cardholder(cardholderJ2)).forceCreate()

// Promise.all([p1, p2]).then((r) => {
//   Cardholder.getList().then((r) => {
//     console.log(r)
//   })
// })


