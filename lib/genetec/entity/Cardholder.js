const Promise = require('bluebird') //Promise.map is available in bluebird

const Entity = require('./Entity')

class Cardholder extends Entity {

  /*
    Class initializer

    @param {object} params - object attributes
      example object
      {
        logicalId: '', 
        firstName: '', 
        lastName: '',
        emailAddress: ''      
      }
    @param {object} opts - additional options(currently not being used)
  */
  constructor(params, opts){
    super(params, opts)
    this.entityType = "Cardholder"
  }

  /*
    Get all cardholder data
    
    @return {promise[array]} array of cardholder objects
      sample return:
      [{
        firstName: '',
        lastName: '',
        email: '',
        guid: '',
        companyName: '',
        floor: '',
        logicalId: ''
      }]
  */
  static getList(){
    return Cardholder.getListGuids().then(Cardholder.getListByGuids)
  }

  /*
    Get all cardholder guids

    @return {promise[array]} array of guid string
      sample return:
      ['guid1', 'guid2']
  */
  static getListGuids(){
    return Entity.listAllByType("Cardholder").then((r) => {
      let guids = []
      let rows = r.rsp.QueryResult[0].Row
      for (var r of rows) {
        r.Cell.forEach((c) => {
          if(c['$'].name == 'Guid'){
            guids.push(c['_'])
          }
        })
      }

      return guids
    })
  }

  /*
    Get cardholder data from list of guids

    @param {array} guids - list of guids
    @return {promise[array]} array of cardholder objects
      sample return:
      [{
        firstName: '',
        lastName: '',
        email: '',
        guid: '',
        companyName: '',
        floor: '',
        logicalId: ''
      }]
  */
  static getListByGuids(guids){
    let promises = Promise.map(
      guids, 
      guid => {
        return Entity.findByGuid(guid).then((r) => {
          let d = r.rsp.entity[0];

          // Get custom fields
          let companyName = null
          let floor = null
          if(d.CustomFields){
            d.CustomFields.forEach((e) => {
              e.item.forEach((item) => {
                let i = item.CustomFieldValue[0]
                let customFieldName = i.CustomField[0].Name[0].toLowerCase()
                if(customFieldName == "companyname"){
                  companyName = i.Value[0]
                }

                if(customFieldName == "floor"){
                  floor = i.Value[0]
                }
              })
            })
          }

          let user = {
            firstName: d.FirstName[0],
            lastName: d.LastName[0],
            email: d.EmailAddress[0],
            guid: d.Guid[0],
            companyName: companyName,
            floor: floor,
            logicalId: d.LogicalId[0]
          }
          return user
        })
      }, 
      { concurrency: 50 }
    )

    return promises
  }
  
}

module.exports = Cardholder
