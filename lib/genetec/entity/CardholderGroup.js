const Entity = require('./Entity')
const Client = require('../lib/Client')

class CardholderGroup extends Entity {

  /*
    Class initializer

    @param {object} params - object attributes
      example object
      {
        logicalId: '', 
        name: '', 
        isVisitorGroup: true  
      }
    @param {object} opts - additional options(currently not being used)
  */
  constructor(params, opts){
    super(params, opts)
    this.entityType = "CardholderGroup"
  }


  /*
    Find a group named "Visitors" and has IsVisitorGroup set to "True"

    @return {promise}
  */
  static async findVisitorGroups(){
    let path = "report/EntityConfiguration"
    let p = {"q": "EntityTypes@CardholderGroup"}
    let self = this

    let promise = new Promise((resolve, reject) => {

      Client.get(path, p).
        then(async(resp)=>{
          // find all guids
          let guids = [];
          resp.rsp.QueryResult[0].Row.forEach((r) => {
            r.Cell.forEach((c) => {
              if(c['$'].name == 'Guid'){
                guids.push(c['_'])
              }
            })
          })

          let array = await Promise.all(guids.map(async(g)=>{
           let v = await self.findByGuid(g)
           return v
          }))

          // console.log("===", array)
          let result = []
          array.forEach((g)=>{
            let entity = g.rsp.entity[0]
            if(entity.IsVisitorGroup == 'True'){
              result.push(entity)
            }
          })

          resolve(result)

        })
    })

    return promise
  }


  /*
    Find a group by given name

    @param {string} groupName

    @return {promise[object]} group object
      example return:
      {
        guid: '',
        logicalId: '',
        name: ''
      }
  */
  static findGroupByName(groupName){
    let path = "report/EntityConfiguration"
    let p = {"q": "EntityTypes@CardholderGroup"}

    let promise = new Promise((resolve, reject) => {

      Client.get(path, p).
        then((resp)=>{
          // find all guids
          let guids = [];
          resp.rsp.QueryResult[0].Row.forEach((r) => {
            r.Cell.forEach((c) => {
              if(c['$'].name == 'Guid'){
                guids.push(c['_'])
              }
            })
          })

          // find visitors guid
          guids.forEach((g) => {
            this.findByGuid(g).
              then(resp => {
                let entity = resp.rsp.entity[0]
                // console.log("[Group] ", entity)
                if(entity.Name[0] == groupName){
                  let r = {
                    guid: g,
                    logicalId: entity.LogicalId[0],
                    name: entity.Name[0]
                  }
                  resolve(r);
                }
                
              })
          })


        })
    })

    return promise
  }

  /*
    Add a cardholder to a group

    @param {string} cardholderType - can be "Visitor" or "Cardholder"
    @param {string} cardholderLogicalIdOrGuid
    @param {string} groupLogicalIdOrGuid

    @return {promise}
  */
  static addCardholderToGroup(cardholderType, cardholderLogicalIdOrGuid, groupLogicalIdOrGuid){

    //
    let promiseFindCardholderGuid = new Promise((resolve, reject) => {
      let cardholderGuid = null

      if(cardholderLogicalIdOrGuid.length >= 32){
        cardholderGuid = Entity.normalizeGuid(cardholderLogicalIdOrGuid)
        resolve(cardholderGuid)
      }
      else{
        Entity.findByLogicalId(cardholderType, cardholderLogicalIdOrGuid).then((r)=>{
          cardholderGuid = r.rsp['entity'][0]['Guid'][0]
          cardholderGuid = Entity.normalizeGuid(cardholderGuid)
          resolve(cardholderGuid)
        })
      }
    })

    //
    let promiseFindGroupGuid = new Promise((resolve, reject) => {
      let groupGuid = null

      if(groupLogicalIdOrGuid.length >= 32){
        groupGuid = Entity.normalizeGuid(groupLogicalIdOrGuid)
        resolve(groupGuid)
      }
      else{
        Entity.findByLogicalId("CardholderGroup", groupLogicalIdOrGuid).then((r)=>{
          groupGuid = r.rsp['entity'][0]['Guid'][0]
          groupGuid = Entity.normalizeGuid(groupGuid)
          resolve(groupGuid)
        })
      }
    })

    //
    return Promise.all([promiseFindCardholderGuid, promiseFindGroupGuid]).then((r) => {
      let cardholderGuid = r[0]
      let groupGuid = r[1]

      let path = "entity"
      let p = {
        "q": `entity=${groupGuid}`,
        "Members@": `{${cardholderGuid}}`
      }

      return Client.post(path, p)
    })
  }
  
}

module.exports = CardholderGroup
