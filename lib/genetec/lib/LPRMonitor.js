const util = require('util')
const fs = require('fs')
const path = require('path')
const EventEmitter = require('events')

const log = require('../../logger.js')
const GenetecLPR = require('../entity/LPR')


class LPRMonitor extends EventEmitter{

  /*
    Class initializer

    @param {object} options
      example object
      {
        dir: ''      
      }
  */
  constructor(options={}){
    super()
    this.options = options
  }

  /*
    Start watching dir for new LPR files
  */
  start(){
    let self = this
    let dir = self.options.dir
    log.info("Start monitoring", dir)
    fs.watch(self.options.dir, { encoding: 'buffer' }, (eventType, filename) => {
      if (filename) {
        let et = eventType.toString()
        if(et == 'change'){
          let fn = filename.toString()
          let fp = path.join(dir, fn)

          if(fs.existsSync(fp)){
            // console.log(`detected file ${fn}`)

            try{

              // make sure file write finished
              let stats = fs.statSync(fp)
              let fileSizeInBytes_old = stats.size
              let size_different =true
              while (size_different) { 
                size_different= !(fs.statSync(fp).size==fileSizeInBytes_old)
              }
              
              // parse xml
              let stream = fs.readFileSync(fp)
              let json = GenetecLPR.parseXML(stream)

              // console.log(json)
              self.emit('lpr-read-detected', json);
            }
            catch(err){
              console.log(err)
            }
            finally{
              try{
                fs.unlinkSync(fp)
              }
              catch(err){
                console.log("error delte file")
              }
            }
          }
          
        }
      }
    });
  }
}


// LPRMonitor.prototype.start = function(options){
//   var self = this;
  
//   this.on('data', function(message){
//     log.trace({data: message}, 'Received Data');

//     //Emit specific actions
//     if (message.action) self.emit(message.action, message);
//   });
// }


module.exports = LPRMonitor
