# SV3 Genetec Web SDK Integration

## Features supported

  - Retrive tenant list
  - Add visitor for access

## How it works

### How to create a visitor for access

  1. Create a visotor
  2. Create a credential, associate it with visitor
  3. Add visitor to the visitor cardholder group (the group should have access rule pre-configured)

