/** AMQPClient

This is the SV3 Client over AMQP.

*/
const util = require('util');
const EventEmitter = require('events');
var amqp = require('amqp');
var log = require('../logger.js').child({module:'AMQPClient'});


/**
 * SV3 AMQP Client
 * @constructor
 */
function AMQPClient(){
	EventEmitter.call(this);
}
util.inherits(AMQPClient, EventEmitter);

/**
 * Connect to the remote server
 * @param {object} options - AMQP connection options
 */
AMQPClient.prototype.connect = function(options, code){
	var self = this;
	this.options = options;

	this.exchange_name = "ccure";
	this.sub_queue_name = "sp2ccure_" + code;
	this.sub_route_key = "sp2ccure_" + code;
	this.pub_route_key = "ccure2sp";

	log.debug({config: options}, "Connecting to AMQP");
	this.mq = amqp.createConnection(options);

	this.mq.on('error', function(err){
		log.error({err: err}, 'AMQP error!');
	});

	this.mq.on('ready', function(){
		log.info('Connected!');
		self.start();
	});

	this.mq.on('close', function(){
		log.warn('AMQP disconnected.');

		if (options.reconnect) {
			setTimeout(5000, self.reconnect);
		}
	});
}

/**
 * Attempt to reconnect automatically
 */
AMQPClient.prototype.reconnect = function(){
	this.connect(this.options);
}

/**
 * Gracefully shut down
 */
AMQPClient.prototype.stop = function(options){
	// Attempt disconnecting (again)
	try {
		this.mq.disconnect();
	} catch (err) {
		log.warn("Tried disconnecting but mq is already Dereference");
	}
	

	// Dereference the exchange
	this.exchange = null;

	// Delete the AMQP client
	delete this.mq;
}

/**
 * Subscribe to remote channels
 */
AMQPClient.prototype.start = function(options){
	var self = this;
	var mq = this.mq;

	// subscribe to sv3 instructions
	this.exchange = mq.exchange(self.exchange_name, {
		durable: true
	}, function(){
		log.debug({name: self.exchange_name}, 'Opened exchange');

		mq.queue(self.sub_queue_name, {
			durable: true,
			autoDelete: false
		}, function(q) {
			log.debug({name: self.sub_queue_name}, 'Queue binded');

			q.bind(self.exchange_name, self.sub_route_key);

			q.subscribe({ ack: true }, function(message) {

				//Emit all messages on the data event
				self.emit('data', message);

				//Acknowledge message
				//TODO: Move to callback, only called when command is completed and requeue if necessary.
				q.shift();
			});
		});
	});

	this.on('data', function(message){
		log.trace({data: message}, 'Received Data');

		//Emit specific actions
		if (message.action) self.emit(message.action, message);
	});
}

AMQPClient.prototype.send = function(data) {
	if (!this.exchange) throw new Error("Not connected!");

	log.trace({data:data, route:this.pub_route_key}, "Publishing...");
	this.exchange.publish(this.pub_route_key, data, {mandatory: true}, function(err){
		if (err) log.error({err:err}, "Sending failed.");
		else log.debug("Sending returned success");
	});
}


module.exports = AMQPClient;


/* Old code for reference

// connect to mq for instructions
var exchange_name = "ccure";
var sub_queue_name = "sp2ccure_" + nconf.get('app.code');
var sub_route_key = "sp2ccure_" + nconf.get('app.code');
var pub_route_key = "ccure2sp";

var mq = null;

function connectToMQ() {
	log.debug("Connecting to AMQP", {config: nconf.get('mq')});

	mq = amqp.createConnection(nconf.get('mq'));

	mq.on('error', function(err) {
		log.error("mq error:" + err);
		setTimeout(function(){
			process.exit(1);
		}, 3000);
	});

	mq.on('close', function() {
		log.error("mq disconnected");
		setTimeout(function(){
			process.exit(1);
		}, 3000);
	});

	mq.on('ready', function() {
		log.debug('Connected to mq');
		// connect to ccure
		ccure.login({
			onSuccess: function() {
				// keep session alive
				if (TaskKeepSessionAlive) {
					clearInterval(TaskKeepSessionAlive);
					TaskKeepSessionAlive = null;
				}
				TaskKeepSessionAlive = setInterval(function() {
					ccure.login({});
				}, config.ccure.sessionRefreshInterval);

				// read card
				if(enableCardRead){
					if (TaskCardRead) {
						clearInterval(TaskCardRead);
						TaskCardRead = null;
					}
					TaskCardRead = setInterval(function() {
						var now = moment();
						var endDate = now.subtract(config.ccure.fetchCardEndFromNow, 'seconds');
						var endStr = endDate.format('MM/DD/YYYY HH:mm:ss');
						var startDate = endDate.subtract(config.ccure.fetchCardDuration, 'seconds');
						var startStr = startDate.format('MM/DD/YYYY HH:mm:ss');
						console.log("read:", startStr, endStr);
						ccure.findAllCardReads({
							fromDate: startStr,
							toDate: endStr,
							onSuccess: function(reads) {
								if (!_.isEmpty(reads)) {
									var r = {
										customer_code: config.app.code,
										data_type: "cardreads",
										data: reads
									};
									_.each(reads, function(i) {
										log.debug("card:", i.card);
									})
									socket.emit("reader:read", r);
								}
							}
						});
					}, config.ccure.fetchCardInteval);
				}

				// subscribe to sv3 instructions
				var exchange = mq.exchange(exchange_name, {
					durable: true
				}, function(){
					mq.queue(sub_queue_name, {
						durable: true,
						autoDelete: false
					}, function(q) {
						log.debug("-----queue binded-----");
						q.bind(exchange_name, sub_route_key);
						q.subscribe({ ack: true }, function(message) {
							try{
								// message = message.toString();
								log.debug(message);
								var json = message;
								if (json.action == 'fetch_directory') {
									ccure.fetchDirectory({
										onSuccess: function(data) {
											var r = {
												customer_code: config.app.code,
												data_type: "directory",
												data: data
											}
											exchange.publish(pub_route_key, r);
										}
									});
								} else if (json.action == 'fetch_clearances') {
									ccure.findClearanceGroups({
										name: json.name,
										onSuccess: function(groups) {
											var g = groups[0];
											ccure.findClearancesForGroup({
												groupID: g.id,
												onSuccess: function(data) {
													var r = {
														customer_code: config.app.code,
														data_type: "clearances",
														data: data
													}
													exchange.publish(pub_route_key, r);
												}
											});
										}
									});
								} else if (json.action == 'fetch_readers') {
									ccure.findDoors({
										onSuccess: function(data) {
											var r = {
												customer_code: config.app.code,
												data_type: "readers",
												data: data
											}
											exchange.publish(pub_route_key, r);
										}
									});
								} else if (json.action == 'create_visitor') {
									var visitorObj = {
										firstName: json.first_name,
										lastName: json.last_name,
										cardNumber: json.card,
										activeDate: json.active_date,
										expiryDate: json.expiry_date,
										clearanceIDs: json.clearance_ids,
										hostPersonnelGuid: json.temp_badge_for_user_guid,
										onSuccess: function(data) {
											//log.debug(data);
										},
										onError: function(err) {
											log.debug(err);
										}
									}
									ccure.checkin(visitorObj);
								} else if (json.action == 'revoke_visitor') {
									var obj = {
										cardNumber: json.card,
										hostPersonnelGuid: json.temp_badge_for_user_guid
									};
									obj.onSuccess = function() {};
									ccure.checkout(obj);
								} else {
								}
							}
							catch(ex){
								log.error("err: ", ex);
							}
							finally {
								q.shift();
							}
						});
					});
				});
			},
			onError: function(body, res, err){
				log.error("ccure login error", err);
				process.exit(1);
			}
		});
	});
}

*/