'use strict'

require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const random = require('../util/random.js');

const Personnel = require('./Personnel.js');

const Client = require('./Client.js');
const Service = require('./Service.js');

const client = new Client();
const service = new Service(client);

const trash = [];

describe('Personnel', function(){

	before(done => {
		client.login((err,token) => {
			if (err) {
				throw new Error("Login error!");
			} else {
				done();
			}
		})
	});

	after(done => {
		//destroy all objects in the trash... in parallel!
		Promise.all(trash.map(i => { return service.destroy(i) }))
		.then(r => {
			done();
		});
	})

	describe('Static properties', function(){
		describe('#TYPE', function(){
			it('should return a string with the full CCURE TypeName', function(){
				expect(Personnel.TYPE).to.be.a('string');
				expect(Personnel.TYPE).to.equal('SoftwareHouse.NextGen.Common.SecurityObjects.Personnel');
			});
		});

		describe('#PROPERTIES', function(){
			it('should return an array with all the property names', function(){
				expect(Personnel.PROPERTIES).to.be.an('array').that.is.not.empty;
			});

			//TODO: add reference to client
			it('should be a subset of the schema properties');
			//client.schema_get(Personnel.PROPERTIES).then((schema) => {
			//	expect(schema).to.include.members(Personnel.PROPERTIES);
			//});
		})

		describe('GUID', function(){
			it('can be passed in the constructor', function(){
				let r = new random();
				let p = new Personnel({
					GUID: r.GUID
				});
				expect(p).to.have.property('GUID', r.GUID);
			});

			it('can be auto generated', function(){
				let p = new Personnel();
				expect(p).to.have.property('GUID').that.is.a('string').lengthOf(36);
			})

			it('should still auto generate if passed as undefined', function(){
				let spObj = {}; //Does not have GUID
				let p = new Personnel({GUID: spObj.GUID});
				expect(p).to.have.property('GUID').that.is.a('string').lengthOf(36);				
			})

			/* Nope.  _.defaults treats null as specific value.  Guess that makes sense. */
			it('should not auto generate if passed as null', function(){
				let spObj = { GUID: null }; //null value for GUID
				let p = new Personnel({GUID: spObj.GUID});
				expect(p).to.have.property('GUID', null);				
			})

			it('can be changed by assigning the property', function() {
				let r = new random();
				let p = new Personnel();

				p.GUID = r.GUID;

				expect(p).to.have.property('GUID', r.GUID);
			})
		});

		describe('CCURE9000', function(){
			it('should accept multple Personnel with the same name', function(){
				let r = new random();

				return Promise.all(
					Array.from(new Array(3), (n,i) => { 
						return service.save(new Personnel({FirstName: r.FirstName, LastName: r.LastName})) 
					})
				).then(result => {
					expect(result).to.be.an('array').that.has.lengthOf(3);
					expect(result[0]).to.be.instanceOf(Personnel);
					expect(result[1]).to.be.instanceOf(Personnel);
					expect(result[2]).to.be.instanceOf(Personnel);

					expect(result[0]).to.have.property('ObjectID').that.is.a('number');
					expect(result[1]).to.have.property('ObjectID').that.is.a('number');
					expect(result[2]).to.have.property('ObjectID').that.is.a('number');
				})
			})

		})
	})
})