'use strict';

/* PersonnelClearancePair Object */

const Ccure9kObject = require('./Ccure9kObject.js');


const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.PersonnelClearancePair'
const PROPERTIES = [
	'ObjectID',
	'GUID',
	'PersonnelID',
	'ClearanceID',
	'StartDateTime',
	'EndDateTime',
	'RequestGUID'
]


class PersonnelClearancePair extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }
	//static get FORMAT_DATETIME () { return FORMAT_DATETIME }
}

module.exports = PersonnelClearancePair;
