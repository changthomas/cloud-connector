'use strict'

require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const moment = require('moment');
const _ = require('lodash');

const Visit = require('./Visit.js');

const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');
const Credential = require('./Credential.js');
const random = require('../util/random.js');

const Client = require('./Client.js');
const Service = require('./Service.js');

const client = new Client();
const service = new Service(client);

const trash = [];

describe('Visit', function(){


	after(done => {
		//destroy all objects in the trash... in parallel!
		Promise.all(trash.map(i => { return service.destroy(i) }))
		.then(r => {
			done();
		});
	})

	describe('Static properties', function(){
		describe('#TYPE', function(){
			it('should return a string with the full CCURE TypeName for Credential', function(){
				expect(Visit.TYPE).to.be.a('string', 'SoftwareHouse.NextGen.Common.SecurityObjects.Visit');
			});
		});

		describe('#PROPERTIES', function(){
			it('should return an array with all the (useful) property names', function(){
				expect(Visit.PROPERTIES).to.be.an('array').that.is.not.empty;
			});

			//TODO: add reference to client
			it('should be a subset of the schema properties');
			//client.schema_get(Personnel.PROPERTIES).then((schema) => {
			//	expect(schema).to.include.members(Personnel.PROPERTIES);
			//});
		})
	});

	describe('Constructor', function(){
		it('should accept no arguments', function(){
			let c = new Credential();
			expect(Object.keys(c)).to.include.members(['CardNumber', 'CHUID'])
		});
		it('should accept some key/values');
	});


	describe('Creating a Visit', function(){
		it('should fail without at least one visitor', function(){
			return service.save(new Visit())
			.then(v => {
				//Should not reach here
				throw new Error("Visit unexpectedly saved without a Visitor!");
			})
			.catch(err => {
				expect(err).to.be.instanceOf(Error)
			})
			.then(() => {
				expect(true).to.be.true;
			})
		})

		it('should succeed when given a visitor', function(){
			return service.save(new Personnel(new random())) //Note Visitor PersonnelType is default (None)
			.then(visitor => {
				let visit = new Visit();
				visit.Visitors.push(visitor);

				return service.save(visit)
			})
		})
	})

	describe('Creating a Visit with Visitor and Host', function(){
		let hosts = [], visitors = [], visits = [];

		before(done => {
			//Create some Hosts and Visitors
			Promise.all(Array.from(new Array(6)).map((n, i) => {
				//Create a Host or Visitor
				return service.save(new Personnel(new random()))
				.then(person => {
					(0 === i%2) ? hosts.push(person): visitors.push(person);
				}).catch(err => {
					//Sometimes people get rejected for nonUniqueEmailAddress. Ignore them
					console.log(err)
				});
			}))
			.then(result => {
				//console.log("\n\nDONE ADDING PEOPLE (%s)\n\n", people.length);
				//TODO: figure out why this gets called before results or people arrays are populated.
				//console.log(result) //Array of undefined values.
				done();
			})
		});

		after(() => {
			trash.push(...hosts, ...visitors, ...visits)
		})

		describe('With one host and one visitor', function(){
			it('should save without error', function() {
				let visit = new Visit();
				visit.Visitors.push(_.sample(visitors));
				visit.Hosts.push(_.sample(hosts));

				return service.save(visit)
				.then(v => {
					expect(v).to.be.instanceOf(Visit);
					expect(v).to.have.property('ObjectID').that.is.a('number');
					expect(v).to.have.property('Visitors').that.is.an('array').that.has.lengthOf(1);
					expect(v).to.have.property('Hosts').that.is.an('array').that.has.lengthOf(1);
				})
			})
		})

		describe('With Multiple hosts and visitors', function(){
			it('should save without error', function() {
				let visit = new Visit();
				visit.Visitors.push(...visitors);
				visit.Hosts.push(...hosts);

				return service.save(visit)
				.then(v => {
					expect(v).to.be.instanceOf(Visit);
					expect(v).to.have.property('ObjectID').that.is.a('number');
					expect(v).to.have.property('Visitors').that.is.an('array').that.has.lengthOf(visitors.length);
					expect(v).to.have.property('Hosts').that.is.an('array').that.has.lengthOf(hosts.length);
				})
			})
		})

	})


})