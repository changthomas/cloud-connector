/* PersonnelType Object */

const Ccure9kObject = require('./Ccure9kObject.js');

const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.PersonnelType'
const PROPERTIES = [
	'ObjectID',
	'Description',
	'GUID',
	'LastModifiedByID',
	'LastModifiedTime',
	'Name',
	'Protected',
	'PartitionID',
	'InactivityPeriod',
	'ExpirationTimespan',
	'ExpirationType',
	'ExpirationSpecificTime',
	'ExpirationBasedOnProperty',
	'ExpirationPropertyClassType',
	'ExpirationIgnoreYear',
	'CanBeVisitor'
]

const SV3_VISITOR_TYPE_TEMPLATE = {
	Name: 'SV3 Visitor',
	Description: 'Visitors managed by SV3',
	CanBeVisitor: true,
	GUID: '6ddf8a0b-a9b1-41fe-9e19-78df1c4e73cc'
};

//TODO: inherit from Ccure9kObject
class PersonnelType extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }

	static get VISITOR_TEMPLATE () { return new PersonnelType(SV3_VISITOR_TYPE_TEMPLATE) }
}

module.exports = PersonnelType;
