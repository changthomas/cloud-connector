/**
 * TestHelper
 * 
 * Contains a number of useful utilities for managing test data in a CCURE test system
 *
**/

'use strict'

const Ccure9kObject = require('../Ccure9kObject.js');
const Personnel = require('../Personnel.js');
const PersonnelType = require('../PersonnelType.js');
const Credential = require('../Credential.js');
const random = require('../../util/random.js');
const _ = require('lodash');


var nextCardNumber = 200000;

const personnelTypeMap = {};

class EmployeeHelper {
	constructor(service) {
		this._service = service;
		this._trash = new DiscerningTrashCan(service);
	}

	get service () {
		return this._service;
	}

	/**
	 * purge()
	 *
	 * Clears all Personnel
	 */
	purge() {
		return this._trash.empty();
	}

	/**
	 * trash()
	 *
	 * Add something else to the trash to be deleted upon purge()
	 *
	 * @param *args anything you want
	**/
	trash(objs){
		this._trash.add(objs)
	}

	/**
	 * employee()
	 *
	 * Generates a new employee
	**/
	employee(properties) {
		return this.getType('Employee')
		.then(pType => {
			let p = new Personnel(new random(properties));
			p.PersonnelTypeID = pType.ObjectID;
			return this.service.save( p );
		}).then(p => {
			this.trash(p); //Add to TRASH.
			return p;
		})
	}

	/**
	 * addCredential()
	 *
	 * Creates a new Credential associated to the given Personnel
	 *
	 * @param personnel Personnel The person to whom the Credential is assigned.
	 * @param cardNumber int The card number to assign
	 */
	addCredential(personnel, cardNumber = nextCardNumber++) {
		return this.service.save( 
			new Credential({
				PersonnelID: personnel.ObjectID,
				CardNumber: cardNumber, 
				Temporary: false,
				Name: `${personnel.Name} Card ${cardNumber}`
			}))
		.then(c => {
			this.trash(c); //Add Credential to trash
			return c;
		})
	}

	/**
	 * populate()
	 * 
	 * Create N personnel with credentials and return the array of personnel.
	 */
	populate(quantity) {
		let _persons = null;

		return Promise.all(Array.from(
			Array(quantity), (n, i) => {
				return this.employee()
			}))
		.then(personnel => {
			_persons = personnel; //maintain reference to personnel

			return Promise.all(
				personnel.map(p => { return this.addCredential(p) })
			)
		}).then(credentials => {
			return _persons;
		})
	}


	getType(name) {
		return new Promise((resolve, reject) => {
			if (personnelTypeMap[name]) { 
				resolve(personnelTypeMap[name])
			} else {
				resolve(
					this.service.findOrCreate(PersonnelType, {Name: name})
					.then(t => {
						personnelTypeMap[name] = t;
						return t;
					})
				)
			}
		})
	}
}


module.exports = EmployeeHelper;


//TODO: resume later
/**
 * DiscerningTrashCan
 * 
 * A better place to put your garbage.
**/
class DiscerningTrashCan {

	constructor(service) {
		this._destroy = [];
		this._unassign = [];
		this.service = service;
	}

	add(...stuff) {
		//Flatten all arrays and take out anything falsy/null/undefined.
		stuff = _.compact(_.flattenDeep(stuff));

		for (let item of stuff) {
			let klass = item.constructor.name
			
			if (klass === "PersonnelClearancePair" || klass === "Credential")
				this._unassign.push(item)
			else
				this._destroy.push(item);
		}
	}

	empty() {
		//Unassign or delete items in an orderly fashion.

		return Promise.all(
			(this._unassign || []).map(c => {
				return this.service.unassign(new Personnel({ObjectID: c.PersonnelID}), [c])
			})
		).then(() => {
			return Promise.all(
				this._destroy.map(i => { return this.service.destroy(i) })
				)
		}).then(() => {
			this.destroy = [];
			this.unassign = [];
		})
	}

}