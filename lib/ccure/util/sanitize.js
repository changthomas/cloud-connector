/* A namespace for sanitization functions */

'use strict';
const _ = require('lodash');

/**
 * sanitizeSearchCriteria prepares a criteria object and priority array for
 * use with findMultiple() and findFirstMatch() by removing any criteria/priority
 * combinations where the criteria value is empty or null.
 *
 * @param {Object} criteria - Object containing name-value pairs to match
 * @param {Array<Array>} priority - Array of arrays of fields to match in order of priority
 * @returns {Array} sanitiedPair - An array containing the sanitized criteria and priority
 *
 * @example
 */
module.exports.sanitizeSearchCriteria = function(criteria, priority) {
	//Remove criteria with ["", null, undefined] values 
	criteria = _.pickBy(criteria, function(k, v) {
		return (v); //omit if truthy ("", null, undefined)
	})

	//Remove priorities with no matching criteria to search for
	priority = priority.filter(function(fields) {
		return fields.some(f => {
			return (criteria[f]) //Keep if truthy criteria value
		});
	});

	//Remove criteria not included in priority list
	criteria = _.pick(criteria, [].concat(...priority));

	return [criteria, priority];
}
