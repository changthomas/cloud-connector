'use strict'

const _ = require('lodash');
const chai = require('chai');
const expect = chai.expect;
const random = require('../../util/random.js');
const sanitize = require('./sanitize.js');

describe('ccure.util.sanitize', function(){

	describe('#sanitizeSearchCriteria()', function(){
		let person, criteria, priority;

		beforeEach(function() {
			person = new random();
			criteria = _.pick(person, [ 'GUID', 'EmailAddress', 'FirstName', 'LastName' ]);
			priority = [['GUID'], ['EmailAddress'], ['FirstName', 'LastName']];
		});

		it('should omit criteria with null values', function() {
			criteria.EmailAddress = null;

			let [result_criteria, result_priority] = sanitize.sanitizeSearchCriteria(criteria, priority);

			expect(result_criteria).to.not.have.property('EmailAddress');
			expect(result_criteria).to.have.property('GUID', criteria.GUID);
			expect(result_criteria).to.have.property('FirstName', criteria.FirstName);
			expect(result_criteria).to.have.property('LastName', criteria.LastName);

			expect(result_priority).to.be.an('array').lengthOf(2);

			let flat = [].concat(...result_priority);

			expect(flat).to.include.members(['GUID', 'FirstName', 'LastName']);
			expect(flat).to.not.have.members(['EmailAddress']);
			expect(flat).to.have.lengthOf(3);
		})
	})
})