
/**
 * Specifically to test the EmployeeService features such as the
 * Temporary badge issuance and revokation.
 */

'use strict'

// Load environment configurations and testing tools
require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const _ = require('lodash');


// Load Service and Client dependencies
const Client = require('../Client.js');
const Service = require('../Service.js');
const EmployeeHelper = require('./EmployeeHelper.js');


// Models
const Personnel = require('../Personnel.js');
const Credential = require('../Credential.js');

// Instances of new CCURE client and service
const client = new Client();
const service = new Service(client);
const helper = new EmployeeHelper(service);

const trash = [];

describe("CCURE9000", function(){
	describe("EmployeeHelper", function(){

		before("Login", function(){
			this.timeout(5000); // Login can take a while
			return service.login();
		})

		describe("#employee()", function() {
			it('Creates a Personnel record', function(){
				return helper.employee()
				.then(p => {
					trash.push(p);

					expect(p).to.have.property('ObjectID').that.is.a('number');
				});
			})
		})

		describe("#addCredential()", function(){
			it('Adds a Credential to a Personnel record', function(){
				return helper.employee()
				.then(p => {
					return Promise.all([p, helper.addCredential(p)]);
				})
				.then(([p,c]) => {
					trash.push(p,c);

					expect(c).to.have.property('ObjectID').that.is.a('number');
					expect(c).to.have.property('PersonnelID').that.is.a('number');
					expect(c).to.have.property('PersonnelID', p.ObjectID);
					expect(c).to.have.property('Temporary', false);
				})
			});
		})

		describe("#populate()", function() {
			it('Creates N new Personnel with 1 credential each', function(){
				return helper.populate(5)
				.then(people => {
					trash.push(...people);

					expect(people).to.be.an('array').lengthOf(5);
					people.forEach(p => {
						expect(p).to.be.instanceOf(Personnel);
						expect(p).to.have.property('ObjectID').that.is.a('number');
					})

					return Promise.all(people.map(p => { return service.find(Credential, {PersonnelID: p.ObjectID}) }))
				})
				.then(credentials => {
					expect(credentials).to.be.an('array').lengthOf(5);

					credentials.forEach(results => {
						expect(results).to.be.an('array').lengthOf(1);
						expect(results[0]).to.be.instanceOf(Credential);
						expect(results[0]).have.property('ObjectID').that.is.a('number');

						trash.push(results[0]);
					});
				})
			});
		});

		describe("#purge()", function(){
			it('Removes all Personnel and Credentials created since initialization.', function(){
				this.timeout(8000);

				return helper.purge()
				.then(() => {
					//expect(helper._trash).to.be.an('array').lengthOf(0);

					//console.log("Spec trash has:",
					//	trash.map(i => { return `${i.constructor.name}:${i.ObjectID}` }) )

					return Promise.all( trash.map(i => { return service.find(i.constructor, {ObjectID:i.ObjectID} ) }) )
				})
				.then(results => {
					results = _.flatten(results);
					if(results.length > 0) {
						console.error("Deleted items are still found!",
							results.map(i => { return `${i.constructor.name}:${i.ObjectID}` }))
					}
					expect(results).to.be.an('array').lengthOf(0);
				})
			})
		})
	})
})
