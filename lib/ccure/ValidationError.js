/**
 * ValidationError
 *
 * Something to throw at silly mistakes.
 *
**/

class ValidationError extends Error {
	constructor(message) {
		super(message);
		this.name = this.constructor.name;
	}
}

module.exports = ValidationError;