'use strict'

// Load environment configurations and testing tools
require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const _ = require('lodash');
const moment = require('moment');
const random = require('../util/random.js');
const config = require('../config.js');


// Load Service and Client dependencies
const Client = require('./Client.js');
const Service = require('./Service.js');
const VisitorService = require('./VisitorService.js');
const EmployeeHelper = require('./util/EmployeeHelper.js');

// Models
const Personnel = require('./Personnel.js');
//const Partition = require('./Partition.js');
const Credential = require('./Credential.js');
const Clearance = require('./Clearance.js');
const PersonnelClearancePair = require('./PersonnelClearancePair.js');
const Visit = require('./Visit.js');

// Instances of new CCURE client and service
const client = new Client();
const service = new Service(client);
const helper = new EmployeeHelper(service);

const visitorService = new VisitorService(service);



describe("VisitorService", function() {

	after("Empty the trash", function(){
		return helper.purge();
	})


	describe("#assignClearances()", function(){

		let clearances, person;

		before("Login", function(){
			this.timeout(6000);
			return service.login();
		})

		beforeEach("Create a person", function(){
			return helper.employee()
			.then(employee => {
				person = employee;
			})
		})

		before("Create a few clearances", function() {
			//Create 5 test clearances
			return Promise.all(Array.from(
				Array(5), (n, i) => {
					return service.save(new Clearance({Name: `TEST Clearance ${i+1} ${moment().format('YYYYMMDD-HHmmss.SSS')}`}))
			}))
			.then(_clearances => {
				helper.trash(..._clearances)
				clearances = _clearances;
			})
		})


		it("Adds a clearance to a person", function() {
			let _clearances = clearances.slice(0,1); //Just use the first Clearance

			return visitorService.assignClearances(person, _clearances.map(c => { return c.ObjectID }))
			.then(() => {
				return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
			})
			.then(pairs => {
				expect(pairs).to.be.an('array').lengthOf(1);
				expect(
					pairs.map(p => {return p.ClearanceID })
				).to.have.all.members(
					_clearances.map(c => { return c.ObjectID })
				);
			});
		});


		it("Adds multiple clearances to a person", function() {
			let _clearances = clearances.slice(0,3); //Use the first 3 clearances

			return visitorService.assignClearances(person, _clearances.map(c => { return c.ObjectID }))
			.then(() => {
				return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
			})
			.then(pairs => {
				expect(pairs).to.be.an('array').lengthOf(3);
				expect(
					pairs.map(p => {return p.ClearanceID })
				).to.have.all.members(
					_clearances.map(c => { return c.ObjectID })
				);
			});
		});


		it("Adds clearances to a person cumulatively", function() {
			let _clearances1 = clearances.slice(0,1); //Use the first 1 clearances
			let _clearances2 = clearances.slice(1,3); //Use the second 2 clearances

			return visitorService.assignClearances(person, _clearances1.map(c => { return c.ObjectID }))
			.then(() => {
				return visitorService.assignClearances(person, _clearances2.map(c => { return c.ObjectID }))
			})
			.then(() => {
				return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
			})
			.then(pairs => {
				expect(pairs).to.be.an('array').lengthOf(3);
				expect(
					pairs.map(p => {return p.ClearanceID })
				).to.have.all.members(
					_clearances1.concat(_clearances2).map(c => { return c.ObjectID })
				);
			});
		});


		it("Adds clearances to a person cumulatively and ignores anything already assigned", function() {
			let _clearances1 = clearances.slice(0,2); //Use the first 2 clearances
			let _clearances2 = clearances.slice(1,3); //Use the second 2 clearances

			return visitorService.assignClearances(person, _clearances1.map(c => { return c.ObjectID }))
			.then(() => {
				return visitorService.assignClearances(person, _clearances2.map(c => { return c.ObjectID }))
			})
			.then(() => {
				return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
			})
			.then(pairs => {
				expect(pairs).to.be.an('array').lengthOf(3);
				expect(
					pairs.map(p => {return p.ClearanceID })
				).to.have.all.members(
					_.union(_clearances1, _clearances2).map(c => { return c.ObjectID })
				);
			});
		});



		it("should remove clearances not specified if remove_extra is true", function() {
			let _clearances1 = clearances.slice(0,3); //Use the first 3 clearances
			let _clearances2 = clearances.slice(2,5); //Use the last clearances
			// There should be an overlap of 1 clearance at index 2 (Test "3" Credential)

			return visitorService.assignClearances(person, _clearances1.map(c => { return c.ObjectID }))
			.then(() => {
				return visitorService.assignClearances(person, _clearances2.map(c => { return c.ObjectID }), true)
			})
			.then(() => {
				return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
			})
			.then(pairs => {
				expect(pairs).to.be.an('array').lengthOf(3);

				let pair_ids = pairs.map(p => {return p.ClearanceID });
				expect( pair_ids ).to.have.all.members(
					_clearances2.map(c => { return c.ObjectID })
				);

				expect( pair_ids ).to.not.have.members(
					_.intersection(_clearances1, _clearances2).map(c => { return c.ObjectID })
				)
			})
		});


	})

	/* Sample ShortpathVisitObject (received from AMQP json):
	{
		first_name: '',
		last_name: '',
		card: 123456,
		active_date: '', //string in format "%m/%d/%Y 00:00:00"
		expiry_date: '',
		clearance_ids: [5001],

		//New fields added to Shortpath v.2.5?.???
		email: 'visit.guest.email',
		guid: 'visit.guest.uuid',
		host: {
			first_name: 'host.first_name',
			last_name:  'host.last_name',
			guid: 		'host.uuid',
			email: 		'host.email || ""',
			login: 		'host.login',
			badge_id: 	'host.badge_id'
		}
		//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
	}
	*/
	describe('#checkin', function(){
		let host, visitor, shortpathVisitObject;

		before("Creating test Host", function() {
			this.timeout(4000)
			let r = new random();

			//Create a host and visitor
			host = new Personnel(r);
			host.Escort = Personnel.ESCORT_OPTION.ESCORT;
			host.PersonnelTypeID = 2; //By default: PersonnelTypeID 2 == Employee, 1 == None, 3 == Contractor, 4 == Visitor
			host.MiddleName = 'Host'

			let test_objs_promises = [
				host,
				new Clearance({Name: `TEST Visitor Clearance A ${moment().format('YYYYMMDD-HHmmss.SSS')}` }),
				new Clearance({Name: `TEST Visitor Clearance B ${moment().format('YYYYMMDD-HHmmss.SSS')}` })
			].map(i => { return service.save(i) });

			return Promise.all(test_objs_promises)
			.then(([host_r, ...clearances]) => {
				//console.log("Created temp host", saved);
				host = host_r;
				helper.trash(host);
				helper.trash(...clearances)

				r = new random(); //Refresh for Visitor

				shortpathVisitObject = {
					first_name: r.firstName,
					last_name: r.lastName,
					card: r.cardNumber,
					active_date: moment().startOf('day').format(Credential.FORMAT_DATETIME), 
					expiry_date: moment().add(1, 'd').endOf('day').format(Credential.FORMAT_DATETIME),
					clearance_ids: clearances.map(c => { return c.ObjectID }),

					//New fields added to Shortpath v.2.5?.???
					email: r.email,
					guid: r.uuid,

					host: {
						first_name: host.FirstName,
						last_name:  host.LastName,
						guid: 		host.GUID,
						email: 		host.EmailAddress,
						login: 		`${host.FirstName}.${host.LastName}`, //not using
						badge_id: 	null //not using, yet (use for temp badge issue?)
					}
					//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
				}
			})
		})


		//it('should find or create a credential with given card number');
		//it('should find or create a Personnel type of Visitor');
		//it('should find or create a Personnel object for the visitor');
		//it('should assign the visitor clearances to the Visitor');
		//it('should find the Host');

		it('should create a Visit object with Host and Visitor checked in', function() {
			config.set('ccure:visit:enabled', true);
			return visitorService.checkin(shortpathVisitObject)
			.then(result => {
				expect(result).to.be.an('array').that.has.lengthOf(5);

				let [visit, card, visitorResult, hostResult, clearances] = result;
				visitor = visitorResult;

				helper.trash(visit, card, visitorResult);

				//Check that a Visit was created and is valid
				expect(visit).to.be.instanceOf(Visit)
				expect(visit).to.have.property('ObjectID').that.is.a('number');

				//Check that the created Visitor matches the Shortpath Visitor data
				expect(visitor).to.be.instanceOf(Personnel);
				expect(visitor).to.have.property('ObjectID').that.is.a('number');
				expect(visitor).to.have.property('GUID', shortpathVisitObject.guid);
				expect(visitor).to.have.property('EmailAddress', shortpathVisitObject.email);
				expect(visitor).to.have.property('FirstName', shortpathVisitObject.first_name);
				expect(visitor).to.have.property('LastName', shortpathVisitObject.last_name);

				//Check that the found host matches the Shortpath host data
				expect(hostResult).to.be.instanceOf(Personnel);
				expect(hostResult).to.have.property('ObjectID').that.is.a('number');
				expect(hostResult).to.have.property('GUID', host.GUID);
				expect(hostResult).to.have.property('EmailAddress', host.EmailAddress);
				expect(hostResult).to.have.property('FirstName', host.FirstName);
				expect(hostResult).to.have.property('LastName', host.LastName);

				//Check that the credential was created
				expect(card).to.be.instanceOf(Credential)
				expect(card).to.have.property('ObjectID').that.is.a('number');
				expect(card).to.have.property('CardNumber', shortpathVisitObject.card);

				return service.find(PersonnelClearancePair, {PersonnelID: visitor.ObjectID})
			}).then(clearancePairs => {
				expect(clearancePairs.map(p => { return _.pick(p, ['PersonnelID', 'ClearanceID'])}))
					.to.have.deep.members(
						shortpathVisitObject.clearance_ids.map(id => {
							return {
								PersonnelID: visitor.ObjectID,
								ClearanceID: id
							}
						})
				);
			})
		})

		it('should find host by email and find visitor by name', function() {
			//TODO: explicitly define config:ccure:visitor:match order to ensure Name priority
			//TODO: explicitly define config:ccure:host:match order to ensure EmailAddress priority
			let clone = {...shortpathVisitObject};
			clone.guid = random.uuid();      //override visitor guid and email with random values (find by name)
			clone.email = random.email();
			clone.host.guid = random.uuid(); //Non-matching host GUID, find by email
			clone.clearance_ids = [];  //will error out if we attempt assiging same clearances to same visitor.
			
			return visitorService.checkin(clone)
			.then(result => {
				expect(result).to.be.an('array').that.has.lengthOf(5);

				let [visit, card, visitorResult, hostResult, clearances] = result;

				helper.trash(visit, card, visitorResult);

				//Check that a Visit was created and is valid
				expect(visit).to.be.instanceOf(Visit)
				expect(visit).to.have.property('ObjectID').that.is.a('number');

				//Check that the returned Visitor is the previous Visitor
				expect(visitorResult).to.be.instanceOf(Personnel);
				expect(visitorResult).to.have.property('ObjectID', visitor.ObjectID);
				expect(visitorResult).to.have.property('GUID').to.not.equal(clone.GUID); //No match
				expect(visitorResult).to.have.property('EmailAddress').to.not.equal(clone.EmailAddress);
				expect(visitorResult).to.have.property('FirstName', visitor.FirstName);
				expect(visitorResult).to.have.property('LastName', visitor.LastName);

				//Check that the returned host is the previous host (or at least the email matches)
				expect(hostResult).to.be.instanceOf(Personnel);
				expect(hostResult).to.have.property('ObjectID').that.is.a('number');
				expect(hostResult).to.have.property('GUID').to.not.equal(clone.host.guid); //No match
				expect(hostResult).to.have.property('GUID', host.GUID);
				expect(hostResult).to.have.property('EmailAddress', host.EmailAddress);
				expect(hostResult).to.have.property('FirstName', host.FirstName);
				expect(hostResult).to.have.property('LastName', host.LastName);

				//Check that the credential was created
				expect(card).to.be.instanceOf(Credential)
				expect(card).to.have.property('ObjectID').that.is.a('number');
				expect(card).to.have.property('CardNumber', shortpathVisitObject.card);
			});
		});


		it('should not fail when the Personnel already has the specified Clearance', function(){
			config.set('ccure:visit:enabled', false); //Disable Visit making for this test.

			let clone = {...shortpathVisitObject};
			
			//clone.clearance_ids = [];  //SHOULD NOT error out if we assiging same clearances to same visitor.
			
			return visitorService.checkin(clone)
			.then(result => {
				expect(result).to.be.an('array').that.has.lengthOf(5);

				let [visit, card, visitorResult, hostResult, clearances] = result;
				helper.trash(visit, card, visitorResult);

				//Check that the credential was created
				expect(card).to.be.instanceOf(Credential)
				expect(card).to.have.property('ObjectID').that.is.a('number');
				expect(card).to.have.property('CardNumber', clone.card);

				//console.log("Clearances returned: ", clearances.map(c => { return `Clearance[${c.ObjectID}][${c.Name}]` }));
				expect(clearances).to.be.an('array').lengthOf(2)
				expect(clearances.map(c => { return c.ObjectID })).to.have.all.members(clone.clearance_ids);
			});
		})

	});
});
