'use strict';

const log = require('../logger.js').child({module: 'CcureService'});
const moment = require('moment');
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');
const Credential = require('./Credential.js');
const Clearance = require('./Clearance.js');
const PersonnelClearancePair = require('./PersonnelClearancePair.js');
const Visit = require('./Visit.js');
const _ = require('lodash');
const config = require('../config.js');
const util = require('./util');
const Criteria = require('./Criteria.js');


class VisitorService {

	constructor(service) {
		this._service = service;
	}

	get service () {
		return this._service;
	}

	/** Get the "SV3 Visitor" PersonnelType, or create it.
	 * New to C*CURE9000 v.2.50 (or 2.40?) is the PersonnelType with CanBeVisitor flag.
	 * "Visitors" in CCURE are Personnel with a PersonnelTypeID of a PersonnelType with the 
	 * CanBeVisitor flag set.
	 *
	 * @returns {Promise<PersonnelType>} The PersonnelType object to use for visitors 
	 */
	findSv3VisitorType() {
		log.debug("findSv3VisitorType() called");

		return new Promise((resolve, reject) => {
			if (this._SV3_VISITOR_PERSONNEL_TYPE) {
				log.debug({result: this._SV3_VISITOR_PERSONNEL_TYPE}, 'findSv3VisitorType() returning cached result.')
				resolve(this._SV3_VISITOR_PERSONNEL_TYPE);
			} else {
				log.debug('findSv3VisitorType() cache miss. Fetching from CCURE.');

				let template = PersonnelType.VISITOR_TEMPLATE;

				//TODO: Replace with service.find() call, dont reference client directly
				this.service.client.objects_get_all_with_criteria({
					TypeFullName: PersonnelType.TYPE,
					WhereClause: 'Name like ? and CanBeVisitor=?',
					Arguments: [template.Name, template.CanBeVisitor]
				})
				.then((types) => {
					if (types.length < 1) {
						log.debug('findSv3VisitorType() CCURE returned empty list.  Creating new Visitor Type.');
						//Empty results; let's create one.
						this.service.findDefaultPartition()
						.then((partition) => {
							template.PartitionID = partition.ObjectID;
							return template;
						})
						.then(() => {
							return this.service.save(template).then((savedObj) => {
								log.debug({result: savedObj}, 'findSv3VisitorType() newly saved CCURE Type for Visitor')
								this._SV3_VISITOR_PERSONNEL_TYPE = savedObj;
								resolve(savedObj);
							})
						})
						.catch(reject);
					} else {
						//TODO: refactor find operation to return type expected.
						resolve(new PersonnelType(types[0])); //Return first matching
					}
				})
				.catch(reject);
			}
		})
	}


	/** Find Or Create Visitor from a Shortpath Visit Object

	    Sample ShortpathVisitObject (received from AMQP json):

		{
			first_name: '',
			last_name: '',
			card: 123456,
			active_date: '', //string in format "%m/%d/%Y 00:00:00"
			expiry_date: '',
			clearance_ids: [5001],

			//New fields added to Shortpath v.2.5?.???
			email: 'visit.guest.email',
			guid: 'visit.guest.uuid',
			host: {
				first_name: 'host.first_name',
				last_name:  'host.last_name',
				guid: 		'host.uuid',
				email: 		'host.email || ""',
				login: 		'host.login',
				badge_id: 	'host.badge_id'
			}
			//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
		}


	 * @param {Object} shortpathVisitObj - The deserialized JSON blob from Shortpath with visitor and host info
	 * @returns {Promise<Personnel>} - The found or created visitor Personnel object
	 */
	findOrCreateVisitor(shortpathVisitObj) {
		const [criteria, priority] = util.sanitize.sanitizeSearchCriteria({
			GUID: shortpathVisitObj.guid,
			EmailAddress: shortpathVisitObj.email,
			FirstName: shortpathVisitObj.first_name,
			LastName: shortpathVisitObj.last_name
		}, config.get('ccure:visitor:match'));

		log.trace({shortpathVisitObj, criteria, priority}, "#findOrCreateVisitor() called")

		return this.findSv3VisitorType()
		.then(visitorType => {
			return Promise.all([
				this.service.findFirstMatch(Personnel, criteria, priority, {PersonnelTypeID: visitorType.ObjectID}),
				visitorType
			])
		})
		.then(([visitor, visitorType]) => {
			// result should be [[visitors], PersonnelType<Visitor>], and visitors may be empty
			return (visitor) ? Promise.resolve(visitor) :
				this.service.save(new Personnel(
					//Remove any empty or null attributes
					_.pickBy({
						//TODO: Should PartitionID be explicitly specified here?
						FirstName: shortpathVisitObj.first_name,
						LastName: shortpathVisitObj.last_name,
						GUID: shortpathVisitObj.guid,
						PersonnelTypeID: visitorType.ObjectID,
						EscortOption: config.get('ccure:visitor:escort') || Personnel.ESCORT_OPTION.ESCORTED_VISITOR,
						EmailAddress: shortpathVisitObj.email
						//NOTE: Email is not guarenteed unique, may cause validation error and not save.
						//TODO: Handle non-unique email error; remove email attribute and try again.
						//TODO: Add support for visitor photo!
					}, function(v, k) { //Careful! _pickBy() calls f(value, key)!
						return (v);
					})
				));
		})
	}



	/* Assign clearances to a visitor and optionally remove any other clearances
	 *
	 * @param {Personnel} personnel - The person to which the credentials will be assigned
	 * @param {Array} clearance_ids - An array of numbers representing the ObjectID of the Credentials to assign
	 * @param {boolean} remove_extra - If true, any Credentials not identified in clearance_ids will be removed
	 * 
	**/
	assignClearances(personnel, clearance_ids, remove_extra=false) {
		return Promise.all([
			this.service.find(PersonnelClearancePair, {PersonnelID: personnel.ObjectID}),
			this.service.find(Clearance, {ObjectID: clearance_ids}) ])
		.then(([pairs, clearances]) => {
			clearance_ids = clearances.map(c => { return c.ObjectID }); //Remove any non-existant IDs
			let assigned_ids = pairs.map(p => { return p.ClearanceID }); //Clearance IDs already assigned

			let ids_to_assign = _.difference(clearance_ids, assigned_ids); //Assign only those not already assigned
			let ids_to_remove = _.difference(assigned_ids, clearance_ids); //Remove those not being assigned

			let pairs_to_remove = _.filter(pairs, p => {
				return _.includes(ids_to_remove, p.ClearanceID)
			})

			return Promise.all([
				clearances,
				this.service.assign(personnel, ids_to_assign.map(cid => {
					return new PersonnelClearancePair({
						PersonnelID: personnel.ObjectID,
						ClearanceID: cid
					})
				})),
				(remove_extra && pairs_to_remove.length > 0) ? this.service.unassign(personnel, pairs_to_remove) : false
			])
		})
		.then(([clearances, ...otherstuff]) => {
			return clearances;
		})
	}


	/* Check-in a visitor from Shortpath

	    Sample ShortpathVisitObject (received from AMQP json):

		{
			first_name: '',
			last_name: '',
			card: 123456,
			active_date: '', //string in format "%m/%d/%Y 00:00:00"
			expiry_date: '',
			clearance_ids: [5001],

			//New fields added to Shortpath v.2.5?.???
			email: 'visit.guest.email',
			guid: 'visit.guest.uuid',
			host: {
				first_name: 'host.first_name',
				last_name:  'host.last_name',
				guid: 		'host.uuid',
				email: 		'host.email || ""',
				login: 		'host.login',
				badge_id: 	'host.badge_id'
			}
			//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
		}
	 *
	 * @param {Object} shortpathVisitObj - Object passed from Shortpath via AMQP.
	 * @returns {Promise<Visit>} The created Visit object, maybe.
	 **/
	checkin(shortpathVisitObj) {
		let visitor, host, cards, card, clearances, visit;

		//TODO: handle case where shortpathVisitObj is missing Visitor UUID and host information. (older versions)

		log.trace({obj:shortpathVisitObj}, "Searching for card, visitor, and host")

		const [criteria, priority] = util.sanitize.sanitizeSearchCriteria({
				GUID: shortpathVisitObj.host.guid,
				EmailAddress: shortpathVisitObj.host.email,
				FirstName: shortpathVisitObj.host.first_name,
				LastName: shortpathVisitObj.host.last_name
			}, config.get('ccure:host:match'))

		//Find the visitor, host, credential, and clearances, then put the pieces together
		return this.findSv3VisitorType()
		.then(visitorType => {
			return Promise.all([
				this.service.find(Credential, {CardNumber: shortpathVisitObj.card}),
				this.findOrCreateVisitor(shortpathVisitObj),
				
				//TODO: Sanitize host matching!!!!
				this.service.findFirstMatch(Personnel, criteria, priority, 
					["PersonnelTypeID != ?", visitorType.ObjectID])
			]);
		})
		.then(result => {
			log.debug({result: result}, "checkin: All Promises returned! (round 1)");

			[cards, visitor, host] = result;

			if (!host && config.get('ccure:visit:enabled')) {
				log.error("Host not found and required for Visit. Aborting.");
				throw new Error("Host was not found!");
			}
			if (!visitor) {
				log.error("Visitor not found and required! Aborting.");
				throw new Error("Visitor cannot be null!");
			}

			//TODO: Don't delete card, just reassign it (once that's been implemented)
			if (cards.length > 0) {
				return this.service.destroy(cards[0]);
			}
		})
		.then(() => {
			card = new Credential({
				CardNumber: shortpathVisitObj.card,
				FacilityCode: shortpathVisitObj.facility_code || config.get('ccure:visitor:facility_code'),
				PersonnelID: visitor.ObjectID,
				ActivationDateTime: shortpathVisitObj.active_date,
				ExpirationDateTime: shortpathVisitObj.expiry_date
			});

			return Promise.all([
				this.service.save(card), 
				this.assignClearances(visitor, shortpathVisitObj.clearance_ids || [], 
					config.get('ccure:visitor:remove_previous_clearances'))
			])
		})
		.then(([_card, _clearances]) => {
			card = _card;
			clearances = _clearances;

			if (config.get('ccure:visit:enabled')) {
				visit = new Visit();
				visit.Visitors.push(visitor);
				visit.Hosts.push(host);
				//visit.Name = `${moment().format('YYYMMDD-HHmmss.SSS')} ${visitor.Name}  ${host.Name}`.substr(0,100);

				return this.service.save(visit)
			}
		})
		.then((visit) => {
			return [visit, card, visitor, host, clearances]
		})
		
	}

}

module.exports = VisitorService;