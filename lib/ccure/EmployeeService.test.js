


/**
 * Specifically to test the directory export feature and ensure data and
 * schema consistency with depricated (v1) function.
 */

'use strict'

// Load environment configurations and testing tools
require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const random = require('../util/random.js');
const moment = require('moment');
const _ = require('lodash');

const EmployeeHelper = require('../util/EmployeeHelper.js');

// Load Service and Client dependencies
const Client = require('./Client.js');
const Service = require('./Service.js');
const EmployeeService = require('./EmployeeService.js');

// Load configuration
//const config = require('../config.js');

// Models
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');


const trash = [];

// Instances of new CCURE client and service
const client = new Client();
const service = new Service(client);

const helper = new EmployeeHelper(service);

/*
directoryService.fetchDirectory()
.then(result => {
	console.log(result);
})
*/


service.login()
.then((token) => {
	console.log("Logged in.");
	
	return helper.employee();
})
.then(p => {
	console.log(`Created ${p.Name} with ID ${p.ObjectID}`);
	return Promise.all([p, helper.addCredential(p)]);
})
.then(([p,c]) => {
	console.log(`Added card ${c.CardNumber} with ID ${c.ObjectID} and name '${c.Name}'`);
	return Promise.all([p, helper.addCredential(p)]);
})
.then(([p,c]) => {
	console.log(`Added card ${c.CardNumber} with ID ${c.ObjectID} and name '${c.Name}'`);
	return helper.purge();
})
.then(() => {
	console.log("Everything Purged.");
})
.catch(err => {
	console.log("Error!", err);
})

/*
.then((employees) => {
	console.log("Sleeping 15s");

	setTimeout(function(){
		purge(employees)
		.catch(err => {
			console.log("ERROR! "+err)
		})
	}, 15000)
})
.catch(err => {
	console.log("ERrror", err);
})
*/