'use strict';

//const Personnel = require('./Personnel.js');
const Credential = require('./Credential.js');

const DISABLED_MARKER = ' SV3DIS';
const TEMPORARY_MARKER = ' SV3TMP';

/* Example Interface from Shortpath
class Pacs::Service
	def visitor_checkin(visit)
		#refactoring of CCURE::BarcodeService#upload(action='activate')
	end

	def visitor_checkout(visit)
		#refactoring of CCURE::BarcodeService#upload(action='revoke')
	end

	def host_issue_temporary_credential(contact, card_number)
		# Disables host lost card(s) and enables temporary card with given card number
	end

	def host_revoke_temporary_credential(contact, card_number)
		# Deletes temporary card and restores original cards
	end
end
*/

class EmployeeService {
	constructor(CcureService) {
		this._service = CcureService;
	}

	get service () {
		return this._service;
	}

	static get DISABLED_MARKER () {
		return DISABLED_MARKER;
	}

	static get TEMPORARY_MARKER () {
		return TEMPORARY_MARKER;
	}


	/**
	 * issueTemporaryCredential()
	 *
	 * Issues a temporary credential to the personnel given, and
	 * disables all other credentials and marks them as lost.
	 *
	 * Can be called multiple times, and will delete any previously issued
	 * temporary credentials.
	 */
	issueTemporaryCredential(personnel, credential) {
		//Find all their current credentials
		return this.service.find(Credential, {PersonnelID: personnel.ObjectID})
		.then(cards => {
			return Promise.all(
				cards.map(c => {
					//If the card is already lost or disabled, ignore it
					if (c.Lost || c.Disabled) return c;
					else if (!c.Disabled && c.Temporary && c.Name.endsWith(TEMPORARY_MARKER)) {
						//Delete any previous temporary credentials
						return this.service.destroy(c);
					} else {
						//Disable all remaining credentials
						c.Lost = true;
						c.Disabled = true;
						c.Name = `${c.Name}${DISABLED_MARKER}`; //Mark Name field
					}
					return this.service.save(c);
				}))
			.then(() => {
				credential.PersonnelID = personnel.ObjectID;
				credential.Name = `${credential.Name}${TEMPORARY_MARKER}`;
				return this.service.save(credential);
			})
		})
	}


	/**
	 * revokeTemporaryCredential()
	 *
	 * Revokes previously issued temporary credential(s) for the personnel given, and
	 * re-enables all other credentials previously disabled.
	 */
	revokeTemporaryCredential(personnel, credential) {
		//Find all their credentials
		return this.service.find(Credential, {PersonnelID: personnel.ObjectID})
		.then(cards => {
			return Promise.all(
				cards.map(c => {
					//Delete the temporary card
					if (!c.Disabled && c.Temporary && c.Name.endsWith(TEMPORARY_MARKER)) {
						return this.service.destroy(c);
					} else if (c.Disabled && c.Lost && c.Name.endsWith(DISABLED_MARKER)) {
						c.Lost = false;
						c.Disabled = false;
						c.Name = c.Name.replace(DISABLED_MARKER, '');
						return this.service.save(c);
					}
					return c; //If it's not one of ours, ignore it.
				}))
		})
	}

}


module.exports = EmployeeService;