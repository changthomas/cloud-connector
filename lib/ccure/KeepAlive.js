'use strict';
const log = require('../logger.js').child({module: 'KeepAlive'});
const PersonnelType = require('./PersonnelType.js');

/**
 * KeepAlive keeps a C*CURE9k Web Service session open by logging in
 * again and again at the specified timeout.  It does not (presently) take into
 * consideration when the last request was made, only when the last keelalive
 * action was taken.
 *
 * @constructor
 * @param {CCURE} client - C*CURE9000 client instance
 * @param {integer} timeout - interval in miliseconds
 * @param {boolean} start - start immediately upon initialization? Defaults to true.
 */
var KeepAlive = function(client, timeout = 120000, start = true) {
	this.client = client;
	this.timeout = timeout;
	this.timer = null; //Declare to prevent stop() from throwing ReferenceError if not started.
	if (start) this.start();
}

/**
 * start running
 * @returns {Boolean} True if started, or false if already running
 */
KeepAlive.prototype.start = function(){
	if (this.running()) return false; //Don't start another timer

	this.timer = setInterval(() => {
		log.trace("KeepAlive timeout reached.");
		this.ping();
	}, this.timeout);

	log.info({timeout: this.timeout}, "Started KeepAlive");

	this.ping();

	return true;
}

KeepAlive.prototype.ping = function() {
	this.client.schema_get(PersonnelType.TYPE)
	.then(function(schema){
		//log.trace({err:err, version:version}, "KeepAlive request returned.");
		log.debug('Connection is alive.');
	})
	.catch(err => {
		log.error({err}, "KeepAlive returned error!")
	})
};


/**
 * stop running
 */
KeepAlive.prototype.stop = function() {
	clearInterval(this.timer);
}
/**
 * check to see if running
 */
KeepAlive.prototype.running = function() {
	return (this.timer !== null) ? true : false;
}

module.exports = KeepAlive;