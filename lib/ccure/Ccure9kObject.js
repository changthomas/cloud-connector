
const ValidationError = require('./ValidationError.js');
const privateData = new WeakMap();

const TYPE = "SV3.CCURE9000.abstract.Ccure9kObject";

class Ccure9kObject {

	//TODO: type is not being used by the base class here.  Remove it?
	constructor(type, properties, obj={}){
		privateData.set(this, {}); //Initialize new empty private property map

		//Assign all the interesting properties to this instance
		//All other properties are ignored.
		for (const p of properties) {
			privateData.get(this)[p] = this[p] = obj[p];
		}

		//Object.freeze(privateData.get(this)); //Assuming all initial properties are flat.
		Object.seal(privateData.get(this))
		//TODO: Consider Object.seal(this) to prevent new properties from being added.
	}

	get Type () { return this.constructor.TYPE }
	set Type (s) {} //Ignore

	static get TYPE() { return TYPE }

	get ClassType () { return this.Type }
	set ClassType (s) {} //Ignore

	get ObjectID () { return privateData.get(this).ObjectID; }
	set ObjectID (i) {
		var o = privateData.get(this);
		if (o.ObjectID) throw new ValidationError("ObjectID is read-only!")
		else o.ObjectID = i;
	}

	/* Update the properties of this object with those of the argument.
	 * @params {Object} attrs
	 * @returns {Ccure9kObject} Returns this object after modifications.
	 */
	update_attributes(attrs) {
		Object.keys(attrs).forEach(k => {
			this[k] = attrs[k];
		});
		return this;
	}

	/**
	 * changes()
	 * 
	 * Returns an associative map of this objects properties that have changed since
	 * being instantiated.  For each property that has changed there is an array containing
	 * the original value (when instantiated) and the new value (changed).  Changes is empty
	 * if there have not been any changes.
	 *
	 * Example:
	 * 
	 * {
	 *   Name: ['Bob', 'Jeff'];
	 * }
	 *
	 * @returns {object} An object representing the changes made since being instantiated.
	 */
	changes() {
		//TODO: return an object whose keys are properties that have changed, and the values are [before, after] tuples
		//returns undefined if there are no changes, or should it return {}?

		let changes = {};
		let original = privateData.get(this);

		Object.keys(this).forEach(k => {
			if (this[k] != original[k] && k != 'ObjectID' && k != 'GUID') {
				changes[k] = [original[k], this[k]]
			}
		})

		return changes;
	}


	/**
	 * isNew()
	 *
	 * Check if this record has ever been saved.
	 *
	 * @returns {boolean} True if the record is new and has never been saved.
	**/
	isNew() {
		return !(this.ObjectID);
	}



	/** Serialize this object for saving.  Note: does not include children objects.
	 *
	 * @params {Array} attrs - (optional) A list of property names to serialize. 
	 *   If specified, limits properties serialized to ones listed.  Otherwise all 
	 *   properties are included.
	 * @returns {object} An object containing the required parameters 
	 * for Client.objects_persist() or Client.objects_put()
	 */
	serialize(attrs) {
		let params = {
			Type: this.Type,
			ObjectID: this.ObjectID,
			PropertyNames: [],
			PropertyValues: [],
		}
		let keys = Object.keys(this);
		if (attrs) keys = keys.filter(k => {
			return attrs.indexOf(k) > -1
		});
		keys.forEach((k) => {
			//If property is not undefined, add it
			if (this[k] !== undefined) {
				params.PropertyNames.push(k);
				params.PropertyValues.push(this[k]);
			}
		})
		return params;
	}
}

module.exports = Ccure9kObject;