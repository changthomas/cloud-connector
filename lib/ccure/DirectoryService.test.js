


/**
 * Specifically to test the directory export feature and ensure data and
 * schema consistency with depricated (v1) function.
 */

'use strict'

// Load environment configurations and testing tools
require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const random = require('../util/random.js');
const moment = require('moment');
const _ = require('lodash');

const TestHelper = require('../util/TestHelper.js');

// Load the old (depricated) ccure library
const CCUREv1 = require('../v1/ccure.js');

// Load Service and Client dependencies
const Client = require('./Client.js');
const Service = require('./Service.js');
const DirectoryService = require('./DirectoryService.js');

// Load configuration
const config = require('../config.js');

// Models
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');

//TODO: Add Groups, PersonnelGroups

const trash = [];

// Instances of new CCURE client and service
const client = new Client();
const service = new Service(client);
const oldccure = new CCUREv1(config.get('ccure:connection'));	//Instance of depricated CCURE client/service

const directoryService = new DirectoryService(service);

const helper = new TestHelper(service);

/*
directoryService.fetchDirectory()
.then(result => {
	console.log(result);
})
*/

service.login()
.then((token) => {
	console.log("Logged in.");
	console.log("\nPurging CCURE9000...")
	return helper.purge()
})
.then(() => {
	console.log("Purged. Starting Population.");

	return helper.populate(50, 5)
})
.catch(err => {
	console.log("ERrror", err);
})
