'use strict';

class Criteria {

	/* format take a variety of criteria formats and outputs in the format of:
	 * ['WHERE string', arg1, arg2, arg3...]
	 *
	 */
	static format (criteria=[]) {
		let where=[], args=[];

		//What kind of criteria was provided? Array or Object?
		if (criteria.constructor === Array) {
			// First element is string WHERE statement
			where = criteria.slice(0,1)
			// Rest of elements are the values
			args = criteria.slice(1);
		} else if (criteria['GUID']) {
			//Special exception for GUID because CCURE API ignores the GUID in the Arguments.  2 hours of my life I will never get back.  WHY!?!?!?!
			where.push(`GUID='${criteria['GUID']}'`)
		} else {
			// Object criteria is a combination of AND and OR operators
			Object.keys(criteria).forEach((k) => {
				if (criteria[k] && k != 'GUID') {
					let values = [].concat(criteria[k]);

					where.push(`(${Array(values.length).fill(`${k}=?`).join(' OR ')})`);
					args.push(...values);
				}
			});
		}

		return [where.join(' AND '), ...args];
	}


	//TODO: Use rest/arguments to support any number of criteria sets
	//TODO: Complete JSDOC params and examples
	/** Combine two or more critera by ANDing the statements together
	 * 
	 */
	static and (crit1, crit2) {
		crit1 = this.format(crit1);
		crit2 = this.format(crit2);

		let where = [crit1.shift(), crit2.shift()].filter(w => w && w.trim().length > 0).join(' AND ')
		return [where].concat(crit1, crit2)
	}

}

module.exports = Criteria;