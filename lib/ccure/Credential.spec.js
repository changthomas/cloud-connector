'use strict'

const chai = require('chai');
const expect = chai.expect;
const Credential = require('./Credential.js');
const random = require('../util/random.js');

describe('Credential', function(){
	describe('Static properties', function(){
		describe('#TYPE', function(){
			it('should return a string with the full CCURE TypeName for Credential', function(){
				expect(Credential.TYPE).to.be.a('string', 'SoftwareHouse.NextGen.Common.SecurityObjects.Credential');
			});
		});

		describe('#PROPERTIES', function(){
			it('should return an array with all the (useful) property names', function(){
				expect(Credential.PROPERTIES).to.be.an('array').that.is.not.empty;
				//include.members doesn't behave as expected...target is superset?
				// expect(Credential.PROPERTIES).to.include.members([
				// 	'CardNumber', 
				// 	'CHUID', 
				// 	'FacilityCode',
				// 	'ActivationDateTime',
				// 	'ExpirationDateTime',
				// 	'Temporary']);
			});

			//TODO: add reference to client
			it('should be a subset of the schema properties');
			//client.schema_get(Personnel.PROPERTIES).then((schema) => {
			//	expect(schema).to.include.members(Personnel.PROPERTIES);
			//});
		})
	});

	describe('Constructor', function(){
		it('should accept no arguments', function(){
			let c = new Credential();
			expect(Object.keys(c)).to.include.members(['CardNumber', 'CHUID'])
		});
		
		it('provided attributes should override defaults', function(){
			//let d = new Credential(); // Defaults

			let name = `Different and random ${random.cardNumber(55500000, 66600000)}`;

			let c = new Credential({
				Name: name,
				Temporary: false
			})

			expect(c).to.have.property('Name', name);
			expect(c).to.have.property('Temporary', false);
		});
	});
})