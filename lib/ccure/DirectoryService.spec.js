
/**
 * Specifically to test the directory export feature and ensure data and
 * schema consistency with depricated (v1) function.
 */

'use strict'

// Load environment configurations and testing tools
require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;

// Load Service and Client dependencies
const Client = require('./Client.js');
const Service = require('./Service.js');
const DirectoryService = require('./DirectoryService.js');
const TestHelper = require('../util/TestHelper.js');


// Models (not currently used)
//const Personnel = require('./Personnel.js');
//const PersonnelType = require('./PersonnelType.js');
//const Partition = require('./Partition.js');

//TODO: Add Groups, PersonnelGroups

// Instances of new CCURE client and service
const client = new Client();
const service = new Service(client);
//onst oldccure = new CCUREv1(config.get('ccure:connection'));	//Instance of depricated CCURE client/service

const directoryService = new DirectoryService(service);
const helper = new TestHelper(service);

const UUIDREGEX = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i


describe("DirectoryService", function(){

	before('Logging in before tests', function(){
		this.timeout(5000);
		return service.login()
	});

	after('Purge all test records', function(){
		return helper.purge();
	})

	describe("#fetchDirectory()", function(){

		const population = 30, groups = 3;

		before('Purge all Personnel and then add test cases', function(){
			this.timeout(20000); //20s timeout. This takes a while

			return helper.purge()
			.then(() => {
				return helper.populate(population, groups);
			})
		});


		let result;

		it('returns a Promise', function(){
			let promise = directoryService.fetchDirectory();

			expect(promise).to.be.an.instanceOf(Promise);

			return promise.then(r => {
				result = r;
				return r;
			});
		});

		it('has personnels', function(){
			expect(result).to.have.property('personnels').that.is.an('Array');
			expect(result.personnels).to.have.lengthOf(population);
			result.personnels.forEach(p => {
				expect(p).to.have.all.keys([
					'id', 'guid', 'first_name', 
					'middle_name', 'last_name',
					'name', 'email', 'escort_option'
					])
			})
		});

		it('has groups', function(){
			expect(result).to.have.property('groups').that.is.an('Array');
			expect(result.groups).to.have.lengthOf(groups);
			result.groups.forEach(p => {
				expect(p).to.have.all.keys([ 'id', 'guid',  'name' ])
			})
		});

		it('has pairs with Personnel and Group GUIDs', function(){
			expect(result).to.have.property('pairs').that.is.an('Array');
			expect(result.pairs).to.have.lengthOf(population);
			result.pairs.forEach(p => {
				expect(p).to.have.all.keys([ 'id', 'guid', 'personnel_id', 
					'group_id', 'personnel_guid', 'group_guid' ])
				expect(p.personnel_guid).to.match(UUIDREGEX)
				expect(p.group_guid).to.match(UUIDREGEX)
			})
		});

		//Call old and new methods, and compare. The data should be the same
		//(up to a few hundred records).  This is probably unnecessary given
		//the tests above.
		it('returns identical results to v1 API');
	});

});



