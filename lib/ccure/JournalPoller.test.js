'use strict'

require('dotenv').config()
const Client = require('./Client.js');
const JournalPoller = require('./JournalPoller.js');


const client = new Client();

client.login(() => { console.log ("Logged in."); });

const jp = new JournalPoller(client, 1000, 2000)

jp.on('event', (data) => { console.log(data) });


jp.subscribe('SoftwareHouse.CrossFire.Common.LogMessageFormats.OperatorLogin');

jp.subscribe('SoftwareHouse.NextGen.Common.LogMessageFormats.CardAdmitted');
jp.subscribe('SoftwareHouse.NextGen.Common.LogMessageFormats.CardRejected');
jp.subscribe('SoftwareHouse.CrossFire.Common.LogMessageFormats.OperatorLogin');
jp.subscribe('SoftwareHouse.CrossFire.Common.LogMessageFormats.OperatorLogin');
jp.subscribe('SoftwareHouse.CrossFire.Common.LogMessageFormats.SystemActivity');

jp.start();




setTimeout(() => {
	console.log("Stopping.");
	jp.stop();
}, 10000);