'use strict'

const chai = require('chai');
const expect = chai.expect;
const Ccure9kObject = require('./Ccure9kObject.js');
const ValidationError = require('./ValidationError.js');

describe('Ccure9kObject', function(){
	describe('Constructor', function(){
		it('should require TYPE, and PROPERTIES arguments');
		it('should optionally take parameters to populate properties');
	})

	describe('#Type property', function(){
		it('should return the TYPE name used when instantiating');
	});


	describe('#update_attributes(params)', function(){
		it('should return the original object with updated attributes from arguments', function(){
			let o = new Ccure9kObject("TEST", ['Name', 'UUID']);
			let o2 = o.update_attributes({Name: 'Charley', CardNumber: 123});
			expect(o).to.equal(o2);
			expect(o).to.have.property('Name', 'Charley');
			expect(o).to.have.property('UUID', undefined);
			expect(o).to.have.property('CardNumber', 123);
		})
	})


	describe('#serialize()', function(){
		
		describe('With a new object for Client.objects_perist()', function(){
			let properties = {
					'Name': 'Bobby',
					'GUID': 'df0361fe-c305-46de-aec4-34a006e4ed07',
					'Color': 'red',
					'Disabled': false
				};
			let o = new Ccure9kObject(
				"TEST",
				['ObjectID', 'Name', 'GUID', 'Color', 'Disabled', 'Lost'],
				properties)

			it('should have all the required fields', function(){
				let result = o.serialize();

				//console.log("Serialized: ", result);

				expect(result).to.have.property('Type').that.is.a('string');
				expect(result).to.have.property('PropertyNames');
				expect(result).to.have.property('PropertyValues');
				expect(result).to.have.property('ObjectID', undefined);

				expect(result.PropertyNames).to.be.an('array').lengthOf(4);
				expect(result.PropertyValues).to.be.an('array').lengthOf(4);

				expect(result.PropertyNames).to.have.members(Object.keys(properties));
				expect(result.PropertyValues).to.have.members(Object.values(properties));
			})
		})


		describe('With an updated object for Client.objects_put()', function(){
			let properties = {
					'ObjectID': 98765,
					'Name': 'Bobby',
					'GUID': 'df0361fe-c305-46de-aec4-34a006e4ed07',
					'Color': 'red',
					'Disabled': false
				};
			let o = new Ccure9kObject(
				"TEST",
				['ObjectID', 'Name', 'GUID', 'Color', 'Disabled', 'Lost'],
				properties)

			it('should include the ObjectID as a named parameter', function(){
				let result = o.serialize();
				expect(result).to.have.property('ObjectID', 98765);
			})

			it('should have only the fields specified', function(){

				o.Color = 'green';
				o.Lost = true;

				let result = o.serialize(Object.keys(o.changes()));

				expect(result).to.have.property('Type').that.is.a('string');
				expect(result).to.have.property('PropertyNames');
				expect(result).to.have.property('PropertyValues');
				expect(result).to.have.property('ObjectID', 98765);

				expect(result.PropertyNames).to.be.an('array').lengthOf(2);
				expect(result.PropertyValues).to.be.an('array').lengthOf(2);

				expect(result.PropertyNames).to.have.members(['Color', 'Lost']);
				expect(result.PropertyValues).to.have.members(['green', true]);
			})
		})


	})


	/**
	 * ObjectID and GUID are unique keys.  ObjectID is the primary key and is
	 * only assigned by CCURE.  GUID can be generated and assigned by a client,
	 * as is the case when adding visitors to CCURE from SV3.
	 *
	 * Client.objects_persist() will use the Ccure9kObject#update_attributes()
	 * to save the ObjectID to an instance of Ccure9kObject when saved to CCURE.
	 * This is the one and ONLY time ObjectID can be assigned.
	 *
	 * GUID should be able to be assigned once when Ccure9kObject is instantiated,
	 * whether that happens by using Service.find() (assigned by CCURE) or when
	 * creating new objects.
	 */
	describe('#ObjectID', function(){
		it('can be assigned when instantiated and not changed after', function(){
			let o = new Ccure9kObject("TEST",
				['ObjectID', 'Name', 'GUID', 'Color', 'Disabled', 'Lost'],
				{
					'ObjectID': 12345,
					'Name': 'Bobby',
					'GUID': 'df0361fe-c305-46de-aec4-34a006e4ed07',
					'Color': 'red',
					'Disabled': false
				})

			expect(() => { o.ObjectID = 54321 }).to.throw(ValidationError);
			expect(o).to.have.property('ObjectID', 12345);
		});

		it('can be assigned once after instantiated if not already assigned', function(){
			let o = new Ccure9kObject("TEST",
				['ObjectID', 'Name', 'GUID', 'Color', 'Disabled', 'Lost'],
				{
					'Name': 'Bobby',
					'GUID': 'df0361fe-c305-46de-aec4-34a006e4ed07',
					'Color': 'red',
					'Disabled': false
				})

			expect(() => { o.ObjectID = 54321 }).to.not.throw(ValidationError);
			expect(o).to.have.property('ObjectID', 54321);

			expect(() => { o.ObjectID = 12345 }).to.throw(ValidationError);
			expect(o).to.have.property('ObjectID', 54321);
		});
	})

	describe('#GUID', function(){
		it('can be assigned when instantiated and not changed after');
		it('can be assigned once after instantiated if not already assigned');
	})


	/**
	 * changes() should return an object like this:
	 *
	 * {
	 *   Name: ['Bob', 'John'],    // Changed Name from Bob to John
	 *   Disabled: [true, false],  // Re-enabled the Credential
	 *   Lost: [false, true],      // Bob/John is now lost
	 *   PersonnelID: [null, 5123] // John was a free man and now belongs to Personnel 5123
	 * }
	 *
	 */
	describe('#changes()', function() {

		let o = null;

		before(function() {
			o = new Ccure9kObject(
				"TEST", 
				['Name', 'GUID', 'Color', 'Disabled', 'Lost'],
				{
					'Name': 'Bobby',
					'GUID': 'df0361fe-c305-46de-aec4-34a006e4ed07',
					'Color': 'red',
					'Disabled': false
				});
		})


		it('should return an associative array of attributes that have changed', function(){
			o.Name = 'Charles';
			o.Lost = true;
			o.Color = 'blue';

			var changes = o.changes();

			expect(changes).to.have.deep.property('Name', ['Bobby', 'Charles']);
			expect(changes).to.have.deep.property('Color', ['red', 'blue']);
			expect(changes).to.have.deep.property('Lost', [undefined, true]);
		})

		/* ObjectID and GUID are now locked and throws TypeError on attempt to change..
		it('should ignore changes to ObjectID or GUID', function(){
			o.ObjectID = (o.ObjectID) ? o.ObjectID + 1 : 9999999;
			o.GUID = '5185af2d-66c4-445f-b207-c0f15b1c5844';

			var changes = o.changes();
			expect(changes).to.not.have.property('ObjectID');
			expect(changes).to.not.have.property('GUID');
		});
		*/
	})

	describe("#isNew()", function(){

		it('Returns TRUE if the record has never been saved', function(){
			//Client returns instantiated object with ObjectID from CCURE, like this
			let o = new Ccure9kObject(
				"TEST", 
				['ObjectID', 'Name', 'GUID', 'Color', 'Disabled', 'Lost'],
				{
					//'ObjectID': 12345,
					'Name': 'Bobby',
					'GUID': 'df0361fe-c305-46de-aec4-34a006e4ed07',
					'Color': 'red',
					'Disabled': false
				});

			expect(o.isNew()).to.be.true;
		});

		it('Returns FALSE if the record has not been saved', function(){
			//Client returns instantiated object with ObjectID from CCURE, like this
			let o = new Ccure9kObject(
				"TEST", 
				['ObjectID', 'Name', 'GUID', 'Color', 'Disabled', 'Lost'],
				{
					'ObjectID': 12345,
					'Name': 'Bobby',
					'GUID': 'df0361fe-c305-46de-aec4-34a006e4ed07',
					'Color': 'red',
					'Disabled': false
				});

			expect(o.isNew()).to.be.false;
		})
	});

})