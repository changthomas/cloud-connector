
/**
 * Specifically to test the EmployeeService features such as the
 * Temporary badge issuance and revokation.
 */

'use strict'

// Load environment configurations and testing tools
require('dotenv').config()
const chai = require('chai');
const expect = chai.expect;
const _ = require('lodash');


// Load Service and Client dependencies
const Client = require('./Client.js');
const Service = require('./Service.js');
const EmployeeService = require('./EmployeeService.js');
const EmployeeHelper = require('./util/EmployeeHelper.js');

// Models
//const Personnel = require('./Personnel.js');
//const Partition = require('./Partition.js');
const Credential = require('./Credential.js');

// Instances of new CCURE client and service
const client = new Client();
const service = new Service(client);
const helper = new EmployeeHelper(service);


const employeeService = new EmployeeService(service);


/* Example Interface from Shortpath
class Pacs::Service
	def visitor_checkin(visit)
		#refactoring of CCURE::BarcodeService#upload(action='activate')
	end

	def visitor_checkout(visit)
		#refactoring of CCURE::BarcodeService#upload(action='revoke')
	end

	def host_issue_temporary_credential(contact, card_number)
		# Disables host lost card(s) and enables temporary card with given card number
	end

	def host_revoke_temporary_credential(contact, card_number)
		# Deletes temporary card and restores original cards
	end
end
*/

describe("CCURE9000", function(){
	describe("EmployeeService", function(){

		before('Logging in before tests', function(){
			this.timeout(5000);
			return service.login()
		});

		let employee1, card1, employee3, cards3;  //Initial state
		let temp1, temp3;                         //Temporary credentials

		before("Create the employees and credentials", function(){
			return Promise.all([
				helper.employee(), //Employee with 1 card
				helper.employee()  //Employee with 3 cards
			])
			.then(([p1, p3]) => {
				employee1 = p1;
				employee3 = p3;

				return Promise.all([
					helper.addCredential(p1),
					Promise.all(Array.from(new Array(3)).map((n, i) => {
							return helper.addCredential(p3)
						}))
					]);
			})
			.then(([c1, c3]) => {
				card1 = c1;
				cards3 = c3;
			})
		})

		after('Delete test employees', function(){
			this.timeout(5000);
			return helper.purge();
		})

		describe("#issueTemporaryCredential()", function(){

			describe("Employee with 1 Credential", function(){

				it("Disabled the existing card and added the new card", function(){
					
					return employeeService.issueTemporaryCredential(employee1, 
						new Credential({CardNumber: 3456789, 
							Temporary: true, Name: `${employee1.Name} Temp 3456789`}) )
					.then(temp => {
						helper.trash(temp);
						temp1 = temp; //reference later
						expect(temp).to.be.instanceOf(Credential);
						expect(temp).to.have.property('ObjectID').that.is.a('number');

						return Promise.all([temp,
							service.find(Credential, {PersonnelID: employee1.ObjectID})
						]);
					})
					.then(([t, cards]) => {
						expect(cards).to.be.an('array').lengthOf(2);

						var original = _.find(cards, {ObjectID: card1.ObjectID});

						expect(original).to.be.instanceOf(Credential);
						expect(original).to.have.property('CardNumber', card1.CardNumber);
						expect(original).to.have.property('ObjectID', card1.ObjectID);
						expect(original).to.have.property('Disabled', true);
						expect(original).to.have.property('Lost', true);
						expect(original.Name).to.have.string(EmployeeService.DISABLED_MARKER)

						var temp = _.find(cards, {ObjectID: t.ObjectID});

						expect(temp).to.be.instanceOf(Credential);
						expect(temp).to.have.property('CardNumber', t.CardNumber);
						expect(temp).to.have.property('Disabled', false);
						expect(temp).to.have.property('Lost', false);
						expect(temp.Name).to.have.string(EmployeeService.TEMPORARY_MARKER);
					})
				})
				
			});

			describe("Employee with 3 Credentials", function(){

				it("Disabled the existing cards and added the new card", function(){
					
					return employeeService.issueTemporaryCredential(employee3, 
						new Credential({CardNumber: 194871910, 
							Temporary: true, Name: `${employee3.Name} Temp 194871910`}) )
					.then(temp => {
						helper.trash(temp);
						temp3 = temp; //reference for later
						expect(temp).to.be.instanceOf(Credential);
						expect(temp).to.have.property('ObjectID').that.is.a('number');
						expect(temp).to.have.property('PersonnelID', employee3.ObjectID);

						return Promise.all([temp,
							service.find(Credential, {PersonnelID: employee3.ObjectID})
						]);
					})
					.then(([t, foundcards]) => {
						expect(foundcards).to.be.an('array').lengthOf(4);

						var [temp, originals] = _.partition(foundcards, {ObjectID: t.ObjectID});

						expect(temp).to.be.an('array').lengthOf(1);
						expect(temp[0]).to.be.instanceOf(Credential);
						expect(temp[0]).to.have.property('CardNumber', t.CardNumber);
						expect(temp[0]).to.have.property('Disabled', false);
						expect(temp[0]).to.have.property('Lost', false);
						expect(temp[0].Name).to.have.string(EmployeeService.TEMPORARY_MARKER);

						expect(originals).to.be.an('array').lengthOf(3);

						originals.forEach(original => {
							expect(original).to.be.instanceOf(Credential);
							expect(original).to.have.property('CardNumber');
							expect(original).to.have.property('Disabled', true);
							expect(original).to.have.property('Lost', true);
							expect(original.Name).to.have.string(EmployeeService.DISABLED_MARKER)							
						})

						//Both collections have the same CardNumbers
						expect( originals.map(c => { return c.CardNumber}) ).to.have.members(
							cards3.map(c => { return c.CardNumber}) );
					})
				})
				
			});

		});


		describe("#revokeTemporaryCredential()", function(){

			describe("Employee with 1 Credential", function(){

				it("Removed the temporary card and enabled the old card", function() {
					return employeeService.revokeTemporaryCredential(employee1, temp1)
					.then(cards => {
						//It returns all of the employee's cards (including deleted ones)
						expect(cards).to.be.an('Array').lengthOf(2);

						let temp = _.find(cards, {ObjectID: temp1.ObjectID});

						expect(temp).to.have.property('deletedAt').that.is.a('Date');

						return service.find(Credential, {PersonnelID: employee1.ObjectID})
					})
					.then(cards => {
						//There should be only 1 card
						expect(cards).to.be.an('Array').lengthOf(1);

						//The card should be the original card
						expect(cards[0]).to.include(_.pick(card1, ['ObjectID', 'CardNumber']));

						//The card should be active
						expect(cards[0]).to.have.property('Disabled', false);
						expect(cards[0]).to.have.property('Lost', false);
						expect(cards[0]).to.have.property('Active', true);

						//The card should not have the disabled marker in the name
						expect(cards[0].Name).to.not.have.string(EmployeeService.DISABLED_MARKER);
					})
				})
			});

			describe("Employee with 3 Credentials", function(){

				it("Removed the temporary card and enabled the old cards", function(){
					return employeeService.revokeTemporaryCredential(employee3, temp3)
					.then(cards => {
						//It returns all of the employee's cards (including deleted ones)
						expect(cards).to.be.an('Array').lengthOf(4);

						let temp = _.find(cards, {ObjectID: temp3.ObjectID});

						expect(temp).to.have.property('deletedAt').that.is.a('Date');

						return service.find(Credential, {PersonnelID: employee3.ObjectID})
					})
					.then(cards => {
						//console.log("Cards:", cards.map(c => { return _.pick(c, ['ObjectID', 'Name', 'CardNumber']) }))
						//There should be 3 cards
						expect(cards).to.be.an('Array').lengthOf(3);

						//The cards should be the original cards
						expect( cards.map(c => { return c.ObjectID}) ).to.have.members(
							cards3.map(c => { return c.ObjectID}) );
						expect( cards.map(c => { return c.CardNumber}) ).to.have.members(
							cards3.map(c => { return c.CardNumber}) );

						cards.forEach(c => {
							//The card should be active
							expect(c).to.have.property('Disabled', false);
							expect(c).to.have.property('Lost', false);
							expect(c).to.have.property('Active', true);

							//The card should not have the disabled marker in the name
							expect(c.Name).to.not.have.string(EmployeeService.DISABLED_MARKER);
						})
					})
				}); //it removed the temporary card...
				
			});

		});

	});//describe EmployeeService

});//describe CCURE9000
