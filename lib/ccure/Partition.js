/* Partition Object */

const Ccure9kObject = require('./Ccure9kObject.js');

/* CCURE9000 v2.60 lists the following for Partition types:
   1. "SoftwareHouse.CrossFire.Common.DataServiceLayer.CorePartition": "Partition",
   2. "SoftwareHouse.CrossFire.Common.Objects.Partition": "Partition1",
   I'm assuming the first is the correct one, and the second possibly depricated.
*/
const TYPE = 'SoftwareHouse.CrossFire.Common.DataServiceLayer.CorePartition'
const PROPERTIES = [
	'ObjectID',
	'Template',
	'Description',
	'GUID',
	'LastModifiedByID',
	'LastModifiedTime',
	'Name',
	'Protected',
	'IsGlobalPartition',
	'IsDefaultPartition'
]


class Partition extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }

}

module.exports = Partition;
