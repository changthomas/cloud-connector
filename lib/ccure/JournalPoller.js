const EventEmitter = require('events');
const _ = require('lodash');
const moment = require('moment');

/**
 * JournalPoller
 *
 * Simply put, the JournalPoller takes a Ccure9k Client object and polls Journal
 * entries at a regular interval.  For each Journal type added to the subscription
 * list, it makes the requests in parallel, manages duplicate results, and emits
 * the entries.
 *
 * Note that the requests intentionally overlap timestamps.  Ccure Journal entries
 * are only returned in the query AFTER being COMMIT to the CCURE SQL database.
 * If the CCURE server is under load, results will get missed.  Thus it is
 * recommended to query for a wider window than you think is necessary to catch
 * events where there is a commit lag.  If you are missing events, increase 
 * the timeframe.
 */
class JournalPoller extends EventEmitter {
	constructor(client, interval=1000, timeframe=2000) {
		super();

		this.client = client;
		this.interval = interval;
		this.timeframe = timeframe;
		this.types = [];
		this.recent = [];

		//Re-emit all events as specific MessageType, eg. OperatorLogin, CardAdmitted, etc.
		this.on('event', (evt) => {
			this.emit(evt.MessageType, evt);
		});
	}

	/**
	 * start() starts pulling operation if it hasn't already been started.
	 */
	start() {
		if (undefined === this.timer) {
			this.timer = setInterval(() => {
				this.poll();
			}, this.interval);

			this.garbage_collector = setInterval(() => {
				this.expire();
			}, Math.ceil(1.5 * this.timeframe));

			return true;
		}
		return false;
	}

	/**
	 * stop() stops polling
	 */
	stop() {
		clearInterval(this.timer);
		clearInterval(this.garbage_collector);
		delete this.timer;
	}

	/**
	 * subscribe() adds the given type to the subscription list for polling
	 */
	subscribe(type) {
		if (_.indexOf(this.types, type) < 0) this.types.push(type);
	}

	/**
	 * unsubscribe() removes a given type from the subscription list and thus
	 * it will no longer be polled.
	 */
	unsubscribe(type) {
		this.types = _.pull(this.types, type);
	}

	/**
	 * poll() queries the client for each subscribed Journal event
	 */
	poll() {
		let start =  moment().subtract(3, 's');
		let end = moment();
		console.log("Poll now %s - %s", start.format('hh:mm:ss'), end.format('hh:mm:ss'));

		for(let i=0; i<this.types.length; i++) {
			this.client.journal_load_historical_messages_basic({
				MessageType: this.types[i],
				FromDate: start,
				ToDate: end
			})
			.then((entries) => {
				this.process(entries);
			})
			.catch((err) => {
				console.log("ERROR!", err);
				throw new Error(err);
				//If invalid type message, remove from types list
			})
		}
	}

	/**
	 * process() takes an array of Journal events and emits those which are new
	 */
	process(entries) {
		if (entries.length < 1) return false;
		console.log("Processing %s entries", entries.length);

		//de-dup and emit
		for(let i=0; i<entries.length; i++) {
			if(undefined === this.recent[entries[i].GUID]) {
				//New event! Emit!
				this.emit('event', entries[i]);	
			}
			//We saw this event.  Update timestamp and ignore.
			this.recent[entries[i].GUID] = new Date().getTime();
		}
	}

	/**
	 * expire() clears the recent cache of all entries older than the timeframe
	 */
	expire() {
		let expired = new Date().getTime() - this.timeframe;
		let keys = Object.keys(this.recent);

		for(let i=0; i<keys.length; i++) {
			if (this.recent[keys[i]] && this.recent[keys[i]] < expired) {
				delete this.recent[keys[i]];
			}
		}
	}

}

module.exports = JournalPoller;

