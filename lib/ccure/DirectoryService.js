'use strict';

const Personnel = require('./Personnel.js');
const Group = require('./Group.js');
const GroupMember = require('./GroupMember.js');

class DirectoryService {
	constructor(CcureService) {
		this._service = CcureService;
	}

	get service () {
		return this._service;
	}


	/**
	 * fetchDirectory()
	 * Drop-in replacement for the v1 fetchDirectory method. This version
	 * replies only on the CcureService module for data access, leverages 
	 * parallel operations, and uses ES6 promises.
	 *
	 * @deprecated since 3.0.0.  New code should rely on methods that return
	 *             defined objects (e.g. Personnel, Group, GroupMember). 
	 */
	fetchDirectory() {

		return Promise.all([
			this.service.findAll(
				Personnel, 
				[
					"EscortOption = ? OR EscortOption = ?", 
					Personnel.ESCORT_OPTION.NONE, 
					Personnel.ESCORT_OPTION.ESCORT
				]),
			this.service.findAll( Group, { GroupType: Personnel.TYPE } )
		])
		.then(([personnel, groups]) => {
			// Transform personnel and groups into Shortpath-expected format
			// Create Group ID -> GUID mapping
			// And final all GroupMembers
			// Create group ID to GUID mapping

			let groups_guid_map = {};

			return Promise.all([
				new Promise((resolve, reject) => {
					resolve(personnel.map(p => {
						return {
							id: p.ObjectID,
							guid: p.GUID,
							first_name: p.FirstName,
							last_name: p.LastName,
							middle_name: p.MiddleName,
							name: p.Name,
							email: p.EmailAddress,
							escort_option: p.EscortOption
						}
					}));
				}), 
				new Promise((resolve, reject) => {
					
					let sp_groups = groups.map(g => {
						groups_guid_map[g.ObjectID] = g.GUID;

						return {
							id: g.ObjectID,
							guid: g.GUID,
							name: g.Name
						}
					})
					resolve(sp_groups);
				}), 
				this.service.findAll( GroupMember, { 
					GroupType: Personnel.TYPE
				}),
				groups_guid_map
			]);
		})
		.then(([personnel, groups, pairs, lookup]) => {
			// Add Personnel.GUID and Group.GUID to Pair object
			// And Format pairs to be in the format Shortpath expects

			return {
				personnels: personnel,
				groups,
				pairs: pairs.map(p => {
					return {
						id: p.ObjectID,
						guid: p.GUID,
						group_id: p.GroupID,
						group_guid: lookup[p.GroupID],
						personnel_id: p.TargetObjectID,
						personnel_guid: p.TargetObjectGUID
					}
				})
			};
		});

	}
}


module.exports = DirectoryService;