'use strict'

require('dotenv').config()
const Client = require('./Client.js');
const Service = require('./Service.js');
const EmployeeHelper = require('./util/EmployeeHelper.js');

//Models
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');
const Credential = require('./Credential.js');
const GroupMember = require('./GroupMember.js');
const Group = require('./Group.js');

//Utils
const random = require('../util/random.js');
const _ = require('lodash')

const client = new Client();
const service = new Service(client);
const helper = new EmployeeHelper(service);


function inspect(card) {
	console.log( _.pick(card, ['ObjectID', 'Status', 'Temporary', 'Active', 'Disabled', 'Lost', 'Stolen']) )
}

/*
service.login()
.then(() => {
	return service.find(Credential, {ObjectID: 5415})
})
.then(([c, ...others]) => {
	inspect(c)
})
*/

service.login()
.then(() => {
	return helper.employee()
})
.then(person => {
	return Promise.all(Array.from(
		Array(3), (n, i) => {
			return helper.addCredential(person)
		}))
})
.then(() => {
	helper.purge();
})
.catch(err => {
	console.error("Something broke:", err)
})

