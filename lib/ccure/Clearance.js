'use strict';

/* Credential Object */

const Ccure9kObject = require('./Ccure9kObject.js');
//const moment = require('moment');
//const _ = require('lodash');

const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.Clearance'
const PROPERTIES = [
	'AssignViaAccessMgmtPortalOnly',
	'OneDoorPerClearance',
	'ExpiringClearancePerPerson',
	'FriendlyName',
	'FriendlyDescription',
	'HasApprovalRule',
	'ApprovalRuleID',
	'ReviewerComment',
	'NextReviewDate',
	'LastReviewedByID',
	'LastReviewDate',
	'ApproversSameAsReviewers',
	'ReviewScheduleID',
	'HasReviewerRule',
	'ReviewRuleID',
	'ObjectID',
	'Name',
	'Description',
	'GUID',
	'Protected',
	'LastModifiedTime',
	'LastModifiedByID',
	'ActivationDate',
	'ExpirationDate',
	'UseActivationDate',
	'UseExpirationDate',
	'PartitionID',
	'Template',
	'Custom'
]
//const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";


class Clearance extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }
	//static get FORMAT_DATETIME () { return FORMAT_DATETIME }
}

module.exports = Clearance;
