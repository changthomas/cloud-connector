'use strict';

const chai = require('chai');
const expect = chai.expect;
const Criteria = require('./Criteria.js');




describe('ccure.Criteria', function(){

	describe('Criteria.format()', function() {

		it('formats objects by ANDing the pairs', function() {
			let output = Criteria.format({
				FirstName: 'First',
				LastName: 'Second'
			});
			expect(output).to.deep.equal(
				[ '(FirstName=?) AND (LastName=?)', 'First', 'Second' ]
			)
		});

		it('formats objects by ANDing pairs and ORing arrays', function(){
			let output = Criteria.format({
				FirstName: 'First',
				LastName: ['Second', 'Third']
			});
			expect(output).to.deep.equal(
				[ '(FirstName=?) AND (LastName=? OR LastName=?)', 'First', 'Second', 'Third' ]
			)
		});

		it('takes [where, ...args] arrays and returns passthrough', function(){
			let output = Criteria.format(["EmailAddress = ? AND PersonnelTypeID != ?", 'a@b.c', 5000]);
			expect(output).to.deep.equal(
				[ 'EmailAddress = ? AND PersonnelTypeID != ?', 'a@b.c', 5000 ]
			)
		});

		it('takes undefined and returns formatted empty criteria', function(){
			let output = Criteria.format();
			expect(output).to.deep.equal(['']);
		});

		it('takes empty array and returns formatted empty criteria', function(){
			let output = Criteria.format([]);
			expect(output).to.deep.equal(['']);
		});

		it('takes empty object and returns formatted empty criteria', function(){
			let output = Criteria.format({});
			expect(output).to.deep.equal(['']);
		});


		it('Copies array values allowing multiple calls with same object(s)', function() {
			const crit = ["PersonnelTypeID != ?", 5000];

			let out1 = Criteria.format(crit);
			let out2 = Criteria.format(crit);

			expect(out1).to.deep.equal(out2);
			expect(out2).to.deep.equal(["PersonnelTypeID != ?", 5000])
		})


	}); // Criteria.format()

	describe('Criteria.and()', function() {

		it('Combines two criteria by ANDing', function(){
			let crit1 = { FirstName: 'First', LastName: ['Second', 'Third'] }
			let crit2 = ["EmailAddress = ? AND PersonnelTypeID != ?", 'a@b.c', 5000]

			let output = Criteria.and(crit1, crit2);

			expect(output).to.deep.equal(
				[ '(FirstName=?) AND (LastName=? OR LastName=?) AND EmailAddress = ? AND PersonnelTypeID != ?', 
				'First', 'Second', 'Third', 'a@b.c', 5000 ]
			)
		});

	})


})


/*
[ '(FirstName=?) AND (LastName=? OR LastName=?)',
  'First',
  'Second',
  'Third' ]
[ 'EmailAddress = ? AND PersonnelTypeID != ?', 'a@b.c', 5000 ]
[ '' ]

*/