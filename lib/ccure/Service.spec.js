'use strict'

require('dotenv').config()
const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;
const random = require('../util/random.js');
const moment = require('moment');
const _ = require('lodash');

const Client = require('./Client.js');
const Service = require('./Service.js');
const EmployeeHelper = require('./util/EmployeeHelper.js');
const VisitorService = require('./VisitorService.js');
const config = require('../config.js');

// Models
const Credential = require('./Credential.js');
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');
const Visit = require('./Visit.js');
const Clearance = require('./Clearance.js');
const PersonnelClearancePair = require('./PersonnelClearancePair.js');


const trash = [];

const client = new Client();
const service = new Service(client);
const employeeHelper = new EmployeeHelper(service);
const visitorService = new VisitorService(service); //TODO: move all tests/references to VisitorService.spec.js

/**
 * createPeople helper function to create test Visitor Personnel Records.
 * Uses random() to generate names and emails, then assigns SV3 Visitor type, 
 *  default Partition, and ESCORTED_VISITOR escort type.
 * 
 * @param {Number} quantity - how many records to create
 * @returns {Array<Personnel>} an array of personnel records.
 */
function createPeople(quantity=30) {
	return Promise.all([
		service.findDefaultPartition(), 
		visitorService.findSv3VisitorType()])
	.then(([partition, visitorType]) => {
		//Create some Personnel and Credentials
		return Promise.all(Array.from(new Array(quantity)).map(i => {
			//Create a Personnel with a Credential
			return service.save(new Personnel(new random({
				EscortOption:Personnel.ESCORT_OPTION.ESCORTED_VISITOR,
				PartitionID: partition.ObjectID,
				PersonnelTypeID: visitorType.ObjectID
			})));
		}));
	});
}

/**
 * createEmployees helper function to create test Visitor Personnel Records.
 * Uses random() to generate names and emails, then assigns SV3 Visitor type, 
 *  default Partition, and ESCORTED_VISITOR escort type.
 * 
 * @param {Number} quantity - how many records to create
 * @returns {Array<Personnel>} an array of personnel records.
 */
function createEmployees(quantity=30) {
	return Promise.all([
		service.findDefaultPartition(), 
		service.findOrCreate(PersonnelType, {Name: 'Employee'})
	])
	.then(([partition, visitorType]) => {
		//Create some Personnel and Credentials
		return Promise.all(Array.from(new Array(quantity)).map(i => {
			//Create a Personnel with a Credential
			return service.save(new Personnel(new random({
				EscortOption:Personnel.ESCORT_OPTION.ESCORT,
				PartitionID: partition.ObjectID,
				PersonnelTypeID: visitorType.ObjectID
			})));
		}));
	});
}

class ShortpathVisit {
	constructor(host, visitor, clearances=[]) {
		this.first_name = visitor.FirstName;
		this.last_name = visitor.LastName;
		this.card = visitor.CardNumber;
		this.active_date = moment().startOf('day').format(Credential.FORMAT_DATETIME);
		this.expiry_date = moment().add(1, 'd').endOf('day').format(Credential.FORMAT_DATETIME)
		this.clearance_ids = clearances.map(c => { return c.ObjectID });
		this.email = visitor.EmailAddress;
		this.guid = visitor.GUID;
		this.host = {
			first_name: host.FirstName,
			last_name:  host.LastName,
			guid: 		host.GUID, //the only important part, really.
			email: 		host.Email, //not using
			login: 		`${host.FirstName}.${host.LastName}`, //not using
			badge_id: 	null //not using
		}
	}
}


describe('Service', function(){

	before('Logging in before tests', function(done){
		//TODO: Consider replacing this with `return service.login()`
		this.timeout(15000);
		client.login((err,token) => {
			if (err) {
				throw new Error("Login error!");
			} else {
				done();
			}
		})
	});

	after('Deleting records created during tests', function() {
		this.timeout(60000); //Why does this take so long?

		//destroy all objects in the trash... in parallel!
		return Promise.all(
			trash.map(i => { 
				return service.destroy(i)
			}))
	})

	describe('Service.client', function(){
		it('should return the client it was instantiated with.', function(){
			let s2 = new Service(client);
			expect(s2.client).to.be.an.instanceOf(Client);
			expect(s2.client).to.equal(client)
		})
	})



	describe('#save()', function(){

		beforeEach("Setup Client Spies", function(){
			sinon.spy(client, 'objects_persist');
			sinon.spy(client, 'objects_put');
		})

		afterEach("Cleanup Spies", function(){
			client.objects_put.restore();
			client.objects_persist.restore();
		})

		describe("Creating new records", function(){

			let host;

			it('should take a new Personnel object and save it', function(){
				return service.save(new Personnel(new random()))
				.then(person => {
					//console.log("Created Person with id %s", person.ObjectID)
					expect(person).to.be.instanceOf(Personnel);
					expect(person).to.have.property('ObjectID').that.is.a('number');
					host = person;
					trash.push(person);

					expect(client.objects_put.called, "Client.objects_put() was not called").to.be.false;
					expect(client.objects_persist.calledOnce, "Client.objects_persist() was called").to.be.true;
				})
			});

			it('should take a new Credential object and save it', function(){
				let c = new Credential({
					CardNumber: random.cardNumber(),
					PersonnelID: host.ObjectID
				});

				return service.save(c)
				.then(card => {
					console.log("Created Credential with id %s and number %s", card.ObjectID, card.CardNumber);
					expect(card).to.be.instanceOf(Credential);
					expect(card).to.have.property('ObjectID').that.is.a('number');
					trash.push(card);

					expect(client.objects_put.called, "Client.objects_put() was not called").to.be.false;
					expect(client.objects_persist.calledOnce, "Client.objects_persist() was called").to.be.true;
				})
			});
		})

		describe('Creating a Clearance', function(){
			it('Should return the Clearance object with its ObjectID assigned', function(){
				let testClearance = new Clearance({Name: 'My Test Clearance'});

				expect(testClearance).to.have.property('ObjectID', undefined);

				return service.save(testClearance)
				.then(c => {
					trash.push(c);

					expect(c).to.be.instanceOf(Clearance);
					expect(c).to.have.property('ObjectID').that.is.a('number');
				})
			})
		})
		
		describe("Updating existing records", function(){
			let card, person;

			before("Create a new Credential", function(){
				return employeeHelper.employee()
				.then(p => {
					person = p;
					return employeeHelper.addCredential(p)
				})
				.then(c => {
					card = c;
				})
			})

			after("Purge test objects", function(){
				return employeeHelper.purge();
			})

			it('should call Client.objects_put() and not Client.objects_persist()', function(){
				card.Name = `New Name ${(new Date()).getTime()}`
				card.Lost = true;

				return service.save(card)
				.then(c => {
					expect(client.objects_put.calledOnce, "Client.objects_put was called once").to.be.true;
					expect(client.objects_persist.called, "Client.objects_persist was not called").to.be.false;
				})
			})
		})

	});


	describe('#find()', function(){
		let people = [], cards = [];

		before('Create some people and cards to find.', function(done){
			this.timeout(5000);


			//Create some Personnel and Credentials
			createPeople(30).then(function(peeps) {
				people = peeps;
				return Promise.all(people.map(p => {
					return service.save(new Credential({
						CardNumber: random.cardNumber(),
						GUID: random.uuid(),
						PersonnelID: p.ObjectID
					}))
				}))
			}).then(creds => {
				cards = creds;
				done();
			}).catch(err => {
				console.log(err);
			})
		});

		after(() => {
			trash.push(...people, ...cards);
		})

		it('should find all instances of a given type (e.g. Partitions)', function(){
			return service.find(Partition)
			.then(result => {
				expect(result).to.be.an('array').that.is.not.empty;
			});
		});

		it('should find specific instances given type and criteria (e.g. Default Partition)', function(){
			return service.find(Partition, {IsDefaultPartition: true})
			.then(result => {
				expect(result).to.be.an('array').lengthOf(1);
			});
		});

		it('should find a Personnel by ObjectID', function(){
			let person = _.sample(people); //Select a random person
			//console.log("%s %s %s", person.ObjectID, person.GUID, person.EmailAddress);

			return service.find(Personnel, {ObjectID: person.ObjectID})
			.then(result => {
				expect(result).to.be.an('array').lengthOf(1);
				expect(result[0]).to.be.instanceOf(Personnel);
				expect(result[0]).to.have.property('ObjectID', person.ObjectID);
				expect(result[0]).to.have.property('GUID', person.GUID);
				expect(result[0]).to.have.property('FirstName', person.FirstName);
				expect(result[0]).to.have.property('LastName', person.LastName);
			})
		})
		
		it('should find a Personnel by GUID', function(){
			let person = _.sample(people); //Select a random person
			//console.log("%s %s %s", person.ObjectID, person.GUID, person.EmailAddress);

			return service.find(Personnel, {GUID: person.GUID})
			.then(result => {
				expect(result).to.be.an('array').lengthOf(1);
				expect(result[0]).to.be.instanceOf(Personnel);
				expect(result[0]).to.have.property('ObjectID', person.ObjectID);
				expect(result[0]).to.have.property('GUID', person.GUID);
				expect(result[0]).to.have.property('FirstName', person.FirstName);
				expect(result[0]).to.have.property('LastName', person.LastName);
			})
		})
		
		it('should find a Personnel by EmailAddress', function(){
			let person = _.sample(people); //Select a random person
			//console.log("%s %s %s", person.ObjectID, person.GUID, person.EmailAddress);

			return service.find(Personnel, {EmailAddress: person.EmailAddress})
			.then(result => {
				expect(result).to.be.an('array').lengthOf(1);
				expect(result[0]).to.be.instanceOf(Personnel);
				expect(result[0]).to.have.property('ObjectID', person.ObjectID);
				expect(result[0]).to.have.property('GUID', person.GUID);
				expect(result[0]).to.have.property('FirstName', person.FirstName);
				expect(result[0]).to.have.property('LastName', person.LastName);
			})
		})


		it('should find multiple people if given multiple IDs to OR with', function(){
			let ids = _.sampleSize(people, 3).map(p => {return p.ObjectID});

			return service.find(Personnel, {ObjectID: ids}) //ids is an array
			.then(result => {
				expect(result).to.be.an('array').lengthOf(3);
				expect(result.map(p => { return p.ObjectID })).to.have.members(ids);
			});
		})
		

		//it('should find a Credential by CardNumber');
	});

	describe('#findOrCreate()', function(){
		const criteria = {Name: 'testFindOrCreate'};

		before('Find a PersonnelType for Special and delete it.', function(done){
			this.timeout(5000);

			service.find(PersonnelType, criteria)
			.then(result => {
				return Promise.all(result.map(r => {
					return service.destroy(r);
				}))
			})
			.then(result => {
				done();
			})
			.catch(err => {
				console.log("Error find/delete testFindOrCreate PersonnelType before()", err);
			});
		});

		it('should create the object when first when not found', function(){
			return service.find(PersonnelType, criteria)
			.then(result => {
				expect(result).to.have.lengthOf(0);

				return service.findOrCreate(PersonnelType, criteria)
			})
			.then(result => {
				expect(result).to.be.instanceOf(PersonnelType);
				Object.keys(criteria).forEach(k => {
					expect(result).to.have.property(k, criteria[k]);					
				})
			})
		});

		it('should return the existing object if one exists', function(){
			return service.find(PersonnelType, criteria)
			.then(results => {
				expect(results).to.be.lengthOf(1);

				return Promise.all([
					results[0],
					service.findOrCreate(PersonnelType, criteria)]);
			})
			.then(([original, newlyfound]) => {

				Object.keys(criteria).forEach(k => {
					expect(original).to.have.property(k, criteria[k]);					
					expect(newlyfound).to.have.property(k, criteria[k]);					
				})
			})
		});
	})

	describe('#destroy()', function(){

		//TODO: break this into multiple tests
		it('should delete an object form CCURE', function(){
			let person = new Personnel(new random());

			return service.save(person)
			.then(result => {
				//console.log("\t\tSaved new Personnel")

				//Check that it was actually saved (has ObjectID and GUID)
				expect(person).to.equal(result);  //should be the same.
				expect(person).to.be.instanceOf(Personnel);

				//Only ObjectID is retured in the status message and added to the Object!
				expect(person).to.have.property('ObjectID').that.is.a('number');

				//Now find the new object
				return service.find(Personnel, {ObjectID: person.ObjectID});
			})
			.then(results => {
				//console.log("\t\tFound newly saved Personnel", results);
				expect(results).to.be.an('array').is.not.empty;
				expect(results[0]).to.be.instanceOf(Personnel);
				expect(results[0]).to.have.property('ObjectID', person.ObjectID);
				//expect(results[0]).to.have.property('GUID').that.is.a('string').lengthOf(36);
				expect(results[0]).to.have.property('GUID', person.GUID); //should be same as created

				person = results[0]; //keep reference

				return service.destroy(person) //Adios sucka!
			})
			.then(obj => {
				//console.log("\t\tDestroyed new Personnel")
				expect(obj).to.be.instanceOf(Personnel);
				expect(obj).to.equal(person); //Should return the same object

				return service.find(Personnel, {ObjectID: person.ObjectID});
			})
			.then(results => {
				//console.log("\t\tSearched again for new Personnel")
				//Last find should not find it again... because its deleted.
				expect(results).to.be.an('array').that.is.empty;
			})
			.catch(err => {
				console.log("ERROR Service.spec.js#destroy() test returned error.", err);
			});
		})
	})


	describe('Dealing with children', function(){
		let person, clearances;

		before('Create a Personnel and some Clearances', function(){
			return employeeHelper.employee()
			.then(p => {
				person = p;
				//Create 5 test clearances
				return Promise.all(Array.from(
					Array(3), (n, i) => {
						return service.save(new Clearance({Name: `TEST Clearance ${i+1} ${moment().format('YYYYMMDD-HHmmss.SSS')}`}))
				})) 
			})
			.then(_clearances => {
				employeeHelper.trash(_clearances)
				clearances = _clearances;
			})
		})

		after('Purge those testy objects', function(){
			return employeeHelper.purge();
		});

		describe("#assign", function(){

			it('should assign children to the parent', function(){
				return service.assign(person, clearances.map(c => { 
					return new PersonnelClearancePair({ PersonnelID: person.ObjectID, ClearanceID: c.ObjectID })
				}))
				.then(() => {
					//Find the pairs for the person
					return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
				})
				.then(pairs => {
					expect(pairs).to.be.an('array').lengthOf(3)
					expect(pairs.map(p => { return p.ClearanceID })).to.have.all.members(
						clearances.map(c => { return c.ObjectID })
						)
				})
				.catch(err => {
					expect(err).to.be.undefined
				})
			});
		});

		describe('#unassign()', function() {

			it('should remove children from the parent', function(){
				return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
				.then(pairs => {
					expect(pairs).to.be.an('array').lengthOf(3)
					return service.unassign(person, pairs)
				})
				.then(() => {
					//Find the pairs for the person
					return service.find(PersonnelClearancePair, {PersonnelID: person.ObjectID})
				})
				.then(pairs => {
					expect(pairs).to.be.an('array').lengthOf(0)
				})
			})
		})
	})


	describe('#findDefaultPartition()', function(){
		it('should always return one Partition marked IsDefaultPartition:true', function(){
			return service.findDefaultPartition().then(result => {
				expect(result).to.be.an.instanceOf(Partition);
				expect(result).to.have.property('IsDefaultPartition', true);
				expect(result).to.have.property('ObjectID').to.be.a('number');
			});
		});
	});


	describe('#findSv3VisitorType()', function(){
		it('should return a PersonnelType object for SV3 Visitors', function(){
			return visitorService.findSv3VisitorType().then(result => {
				expect(result).to.be.an.instanceOf(PersonnelType);
				expect(result).to.have.property('Name', 'SV3 Visitor');
				expect(result).to.have.property('CanBeVisitor', true);
				expect(result).to.have.property('ObjectID').that.is.a('number');

				//TODO: uncomment this line
				//trash.push(result); //Trash it after all tests complete.
			});
		});

		//TODO: complete this test.
		it('should create one if its missing');
	});


	describe('#findOrCreateVisitor', function(){
		let host, visitor, shortpathVisitObject;

		//The shortpathVisitObject coming from Shortpath has reference to a host
		//employee which is assumed to have a matching host in CCURE.  While findOrCreateVisitor()
		//does not require a host, we create one here such that the shortpathVisitObject is as
		//close to real as possible.  See also #checkin()
		before(function(done){
			//Create a host and visitor
			host = new Personnel(new random());
			host.Escort = Personnel.ESCORT_OPTION.ESCORT;
			host.PersonnelTypeID = 2; //By default: PersonnelTypeID 2 == Employee, 1 == None, 3 == Contractor, 4 == Visitor
			host.MiddleName = 'Host'

			service.save(host).then(saved => {
				//console.log("Created temp host", saved);
				host = saved;
				trash.push(host);

				let visitor = new random(); //Refresh for Visitor

				shortpathVisitObject = new ShortpathVisit(host, visitor); //clearances not needed

				/* Old method:
				shortpathVisitObject = {
					first_name: r.firstName,
					last_name: r.lastName,
					card: r.cardNumber,
					active_date: moment().startOf('day').format(Credential.FORMAT_DATETIME), 
					expiry_date: moment().add(1, 'd').endOf('day').format(Credential.FORMAT_DATETIME),
					clearance_ids: [5001], //double check this is an array.

					//New fields added to Shortpath v.2.5?.???
					email: r.email, //not using yet
					guid: r.uuid,   //will likely be null

					host: {
						first_name: host.FirstName,
						last_name:  host.LastName,
						guid: 		host.GUID, //the only important part, really.
						email: 		host.Email, //not using
						login: 		`${host.FirstName}.${host.LastName}`, //not using
						badge_id: 	null //not using
					}
					//temp_badge_for_user_guid: 'some uuid goes here', //This is only used for temp employee bagde. Ignore for now?
				} */

				done();
			}).catch(err => { throw err });
		});

		it('should create a new Personnel record the first time', function(){
			return visitorService.findOrCreateVisitor(shortpathVisitObject)
			.then(v => {
				visitor = v; //retain reference to saved Personnel record for comparison later
				expect(v).to.be.instanceOf(Personnel);
				expect(v).to.have.property('ObjectID');
				expect(v).to.have.property('GUID', shortpathVisitObject.guid);
				expect(v).to.have.property('EmailAddress', shortpathVisitObject.email);
				expect(v).to.have.property('FirstName', shortpathVisitObject.first_name);
				expect(v).to.have.property('LastName', shortpathVisitObject.last_name);
			})
		});

		it('should find the same person matching GUID', function(){
			return visitorService.findOrCreateVisitor(shortpathVisitObject)
			.then(v => {
				expect(v).to.be.instanceOf(Personnel);
				expect(v).to.have.property('ObjectID', visitor.ObjectID);
				expect(v).to.have.property('GUID', visitor.GUID);
				expect(v).to.have.property('EmailAddress', shortpathVisitObject.email);
				expect(v).to.have.property('FirstName', shortpathVisitObject.first_name);
				expect(v).to.have.property('LastName', shortpathVisitObject.last_name);
			})
		});

		//Shortpath isn't sure this is the same person (different UUID), but this 
		//customer is confident that it's the same person if the email address matches.
		//TODO: set config:ccure:visitor:match order explicitly here to:
		// [['GUID'], ['EmailAddress'],['FirstName', 'LastName']]
		it('should find the same person matching Email and differing GUID', function(){
			//Shallow clone/merge operation using ES6 spread operator:
			/* eslint-disable-next-line */
			let clone = {...shortpathVisitObject, ...{guid:random.uuid()}}
			return visitorService.findOrCreateVisitor(clone)
			.then(v => {
				expect(v).to.be.instanceOf(Personnel);
				expect(v).to.have.property('ObjectID', visitor.ObjectID);
				expect(v).to.have.property('GUID').to.not.equal(clone.guid);
				expect(v).to.have.property('EmailAddress', shortpathVisitObject.email);
				expect(v).to.have.property('FirstName', shortpathVisitObject.first_name);
				expect(v).to.have.property('LastName', shortpathVisitObject.last_name);
			})
		})

		//Shortpath isn't sure this is the same person (different UUID and Email), but this 
		//customer is confident that it's the same person if the full name matches.
		//TODO: set config:ccure:visitor:match order explicitly here to:
		// [['GUID'], ['EmailAddress'],['FirstName', 'LastName']]
		it('should find the same person matching First and Last Name and differing GUID and Email', function(){
			//Shallow clone/merge operation using ES6 spread operator:
			let clone = {...shortpathVisitObject, ...{guid:random.uuid(), email:random.email()}}
			return visitorService.findOrCreateVisitor(clone)
			.then(v => {
				expect(v).to.be.instanceOf(Personnel);
				expect(v).to.have.property('ObjectID', visitor.ObjectID);
				expect(v).to.have.property('GUID').to.not.equal(clone.guid);
				expect(v).to.have.property('EmailAddress').to.not.equal(clone.EmailAddress)
				expect(v).to.have.property('FirstName', shortpathVisitObject.first_name);
				expect(v).to.have.property('LastName', shortpathVisitObject.last_name);
			})
		});

		it('should create visitors with empty string email address', function(){
			let visitor = new random();
			let visit = new ShortpathVisit(host, visitor);
			visit.email = '';

			return visitorService.findOrCreateVisitor(visit)
			.then(v => {
				expect(v).to.be.instanceOf(Personnel);
				expect(v).to.have.property('ObjectID').that.is.a('number');
				expect(v).to.have.property('GUID');
				expect(v).to.have.property('EmailAddress').that.is.undefined;
				expect(v).to.have.property('FirstName', visitor.FirstName);
				expect(v).to.have.property('LastName', visitor.LastName);
			});
		});
	})
	


	describe('Parallel Find Methods', function() {
		let people, labrats;

		before('Creating people to find', function(done) {
			this.timeout(3000);
			createPeople(30).then((p) => {
				people = p;
				trash.push(...people);
				done();
			})
		});

		beforeEach('Select some lab rats', function() {
			labrats = _.sampleSize(people, 3);
		});

		describe('#findMultiple()', function() {
			it('should return an array of arrays of matching Personnel in the order expected', function() {
				//Should return labrats in 2, 0 ,1 order.
				return service.findMultiple(Personnel, {
					GUID: labrats[2].GUID,
					EmailAddress: labrats[0].EmailAddress,
					FirstName: labrats[1].FirstName,
					LastName: labrats[1].LastName
				}, [['GUID'], ['EmailAddress'], ['FirstName', 'LastName']])
				.then(results => {
					expect(results).to.be.an('array').lengthOf(3);
					//Expand and test.
					let [two, zero, one, ...other] = results;
					
					expect(zero).to.be.an('array').lengthOf(1);
					expect(one).to.be.an('array').lengthOf(1);
					expect(two).to.be.an('array').lengthOf(1);
					expect(other).to.be.an('array').that.is.empty;

					[two, zero, one] = [...two, ...zero, ...one];

					expect(two).to.have.property('GUID', labrats[2].GUID);
					expect(zero).to.have.property('EmailAddress', labrats[0].EmailAddress);
					expect(one).to.have.property('FirstName', labrats[1].FirstName);
					expect(one).to.have.property('LastName', labrats[1].LastName);
				})
			});

			it('should return empty arrays where no results were found');
		});

		describe('#findFirstMatch()', function() {
			it('should return the first matching Personnel given the priority (GUID)', function() {
				return service.findFirstMatch(Personnel, {
					GUID: labrats[2].GUID,
					EmailAddress: labrats[0].EmailAddress,
					FirstName: labrats[1].FirstName,
					LastName: labrats[1].LastName
				}, [['GUID'], ['EmailAddress'], ['FirstName', 'LastName']])
				.then(result => {
					expect(result).to.be.an.instanceOf(Personnel);
					expect(result).to.have.property('GUID', labrats[2].GUID);
					expect(result).to.have.property('EmailAddress', labrats[2].EmailAddress);
					expect(result).to.have.property('FirstName', labrats[2].FirstName);
					expect(result).to.have.property('LastName', labrats[2].LastName);
				})
			});
			it('should return the first matching Personnel given the priority (Email)', function() {
				return service.findFirstMatch(Personnel, {
					GUID: random.uuid(), //will not match
					EmailAddress: labrats[0].EmailAddress,
					FirstName: labrats[1].FirstName,
					LastName: labrats[1].LastName
				}, [['GUID'], ['EmailAddress'], ['FirstName', 'LastName']])
				.then(result => {
					expect(result).to.be.an.instanceOf(Personnel);
					expect(result).to.have.property('GUID', labrats[0].GUID);
					expect(result).to.have.property('EmailAddress', labrats[0].EmailAddress);
					expect(result).to.have.property('FirstName', labrats[0].FirstName);
					expect(result).to.have.property('LastName', labrats[0].LastName);
				})
			});
			it('should return the first matching Personnel given the priority (Name)', function() {
				return service.findFirstMatch(Personnel, {
					GUID: random.uuid(), //will not match
					EmailAddress: random.email(),
					FirstName: labrats[1].FirstName,
					LastName: labrats[1].LastName
				}, [['GUID'], ['EmailAddress'], ['FirstName', 'LastName']])
				.then(result => {
					expect(result).to.be.an.instanceOf(Personnel);
					expect(result).to.have.property('GUID', labrats[1].GUID);
					expect(result).to.have.property('EmailAddress', labrats[1].EmailAddress);
					expect(result).to.have.property('FirstName', labrats[1].FirstName);
					expect(result).to.have.property('LastName', labrats[1].LastName);
				})
			});

			it('should return undefined if nothing matches', function() {
				return service.findFirstMatch(Personnel, {
					GUID: random.uuid(), //will not match
					EmailAddress: random.email() //will not match.
				}, [['GUID'], ['EmailAddress']])
				.then(result => {
					expect(result).to.be.undefined;
				})
			});
			/*
			 * On second thought, the service should just return search results matching 
			 * whatever was provided, without consideration of invalid data or user error.
			 * Don't include this test.
			it('should skip searching for null email', function() {
				return service.findFirstMatch(Personnel, {
					GUID: random.uuid(), //will not match
					EmailAddress: null, //should not match
					FirstName: labrats[2].FirstName,
					LastName: labrats[2].LastName
				}, [['GUID'], ['EmailAddress'], ['FirstName', 'LastName']])
				.then(result => {
					expect(result).to.be.an.instanceOf(Personnel);
					expect(result).to.have.property('GUID', labrats[2].GUID);
					expect(result).to.have.property('EmailAddress', labrats[2].EmailAddress);
					expect(result).to.have.property('FirstName', labrats[2].FirstName);
					expect(result).to.have.property('LastName', labrats[2].LastName);
				})
			});
			*/
		});
	});




	//TODO: Complete this test. Currently causes CCURE errors
	/*
	describe('Creating Credentials') , function() {
		let employees;

		before(function() {
			return createEmployees()
			.then(result => {
				employees = result;
			})
		})

		it('Can be constructed', function(){
			return service.save(new Credential({
				CardNumber: 55555,
				FacilityCode: 13,
				Name: 'Sample Employee Badge 55555'
			}))
			.then(c => {
				expect(c).to.be.a('Credential');
				expect(c).to.have.property('CardNumber', 55555);
			})
		})
	}) */

});
