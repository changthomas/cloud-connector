"use strict";

/* VisitVisitorPair Object */

/* Interesting CCURE types that may be related:
    "SoftwareHouse.NextGen.Common.SecurityObjects.CheckOutVisitorsAndReturnBadgeDoorActionItem": "Check Out Visitors And Return Badge",
    "SoftwareHouse.NextGen.Common.SecurityObjects.CheckInVisitorsDoorActionItem": "Check Visitors In",
    "SoftwareHouse.NextGen.Common.SecurityObjects.CheckOutVisitorsDoorActionItem": "Check Visitors Out",
    "SoftwareHouse.NextGen.Common.SecurityObjects.CheckInSite": "Check-in Site",
    "SoftwareHouse.NextGen.Common.SecurityObjects.CheckInSiteVisitSitePair": "Check-in Site Visit Site Pair",
    "SoftwareHouse.NextGen.Common.SecurityObjects.CheckInSiteVisitorVisitSiteView": "CheckInSiteVisitorVisitSiteView",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteDocument": "Private Document",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteDocumentPair": "Shared Document1",

    "SoftwareHouse.NextGen.Common.SecurityObjects.Visit": "Visit",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitClearancePair": "Visit Clearance Pair",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitDocument": "Visit Document",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitHostPair": "Visit Host Pair",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSite": "Visit Site",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteHostPair": "Visit Site Host",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteHostView": "Visit Site Host List",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteVisitTemplatePair": "Visit Template",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitVisitorPair": "Visit Visitor Pair",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteVisitorFieldList": "Visitor Field List",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteVisitorView": "VisitSiteVisitorView",
    "SoftwareHouse.NextGen.Common.SecurityObjects.VisitSiteVisitUDFFieldList": "VisitSiteVisitUDFFieldList",
*/

const Ccure9kObject = require('./Ccure9kObject.js');
const moment = require('moment');
const _ = require('lodash');

const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.VisitVisitorPair'
const PROPERTIES = [
	'ObjectID',
	'VisitID',
	'PersonnelID',
	'CheckInTime',
	'CheckOutTime'
]
const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";


class VisitVisitorPair extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);

		//Fill in Defaults
		_.defaults(this, {
			CheckInTime: moment().format(FORMAT_DATETIME),
		});
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }

}

module.exports = VisitVisitorPair;
