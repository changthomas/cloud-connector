"use strict";

/** Service
 *  Implements Facade pattern to simplify operations that require multiple calls
 *
*/

const log = require('../logger.js').child({module: 'CcureService'});
const moment = require('moment');
const Ccure9kObject = require('./Ccure9kObject.js');
const Personnel = require('./Personnel.js');
const PersonnelType = require('./PersonnelType.js');
const Partition = require('./Partition.js');
const Credential = require('./Credential.js');
const Clearance = require('./Clearance.js');
const PersonnelClearancePair = require('./PersonnelClearancePair.js');
const Visit = require('./Visit.js');
const JournalPoller = require('./JournalPoller.js');
const _ = require('lodash');
const config = require('../config.js');
const util = require('./util');
const Criteria = require('./Criteria.js');

const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";



class CcureService {

	constructor(client) {
		this._client = client;
	}

	get client () {
		return this._client;
	}

	login() {
		return new Promise((resolve, reject) => {
			this.client.login(function(err, token) {
				if (err) {
					reject(err);
				} else {
					resolve(token);
				}
			});

		})
	}

	/**
	 * version()
	 * 
	 * Queries remote CCURE9000 version.
	 */
	version() {
		return new Promise((resolve, reject) => {
			this.client.generic_versions((err, ver) => {
				if (err) reject(err)
				else resolve(ver);
			});
		})
	}

/* CRUD Operations: Save (create/update), Find (read), and Destroy (delete) */

	/** Create or Update a Ccure9kObject, as determined by whether the object was
	 *  loaded from CCURE or is newly instantiated, respectively.
	 *
	 * @params {Ccure9kObject} obj - The object to save. Must extend Ccure9kObject or implement #serialize()
	 * @returns {Promise} Promise for the action.  Resolve handler passes the saved object back (with ObjectID populated)
	 */
	save(obj) {
		//TODO: Throw error if the object has been deleted and we are trying to save again.
		return (obj.isNew()) ? this.create(obj) : this.update(obj);
	}

	create(obj) {
		return this.client.objects_persist(obj.serialize())
		.then((result) => {
			return obj.update_attributes(result);
		})
	}

	update(obj) {
		return this.client.objects_put(obj.serialize(Object.keys(obj.changes())))
		.then(r => { return obj });
	}

	/**
	 * Assign one or more children to a parent.  This actually creates the Children and
	 * assigns them to the parent only if the child does not already exist, and the child
	 * has a "belongs_to" relationship (one of its properties is a foreign key to the parent).
	 * This method does not support many-to-many relationships, or assiging children that
	 * already exist.
	 *
	 * @param {Ccure9kObject} parent - The parent object to which the Children will be added
	 * @param {Array<Ccure9kObject>} Children - The children to assign.
	 * @returns {Promise}
	 */
	assign(parent, children=[]) {
		return this.client.objects_persist_to_container(
			parent.Type,
			parent.ObjectID, 
			[].concat(children).map(c => {
				return c.serialize(); //Ensure that Children is Array, then serialize them all
			})
		);
	}


	/**
	 * Does the opposite of assign.  Takes a parent object and removes the given children.
	 * The CCURE Web Service does not allow direct deleting of join objects, such as the
	 * PersonnelClearancePair.  It also does not allow removing the association between 
	 * Personnel and Credentials.  Thus there is the unassign() method.  This should allow
	 * you to remove the association between objects without deleting the parent or child.
	 *
	 * @param {Ccure9kObject} parent - The parent object to which the Children will be added
	 * @param {Array<Ccure9kObject>} Children - The children to remove.
	 * @returns {Promise}
	 */
	unassign(parent, children=[]) {
		return this.client.objects_remove_from_container(
			parent.Type,
			parent.ObjectID, 
			[].concat(children).map(c => { //Ensure that Children is Array,
				//Special serialization object just for objects_remove_from_container. Ugh.
				return {Type: c.Type, ID: c.ObjectID};
			})
		);
	}

	/** Find objects of a given type matching (optional) criteria.
	 * Criteria is a simple exact match using all criteria (concats all 
	 * parameters using AND statement).  This is a basic wrapper around
	 * Client.objects_get_all_with_criteria().  The major difference is
	 * that find() will return an array of instantiated objects of the
	 * expected type.
	 * 
	 * @param {Ccure9kObject SubClass} type - The object type you wish to find
	 * @param {Object} criteria - Property names and corresponding values
	 */
	find(typeObj, criteria=[], limit=1000, page=1) {
		
		//Format criteria Rails style (["WHERE STATEMENT", param1, param2, param3, ...])
		criteria = Criteria.format(criteria);

		let params = {
			TypeFullName: typeObj.TYPE,
			WhereClause: criteria.shift(),
			Arguments: criteria,
			PageSize: limit,
			PageNumber: page
		};
		//Keep this here in case there are more fun exceptions like GUID
		//log.trace(params, "Find is about to objects_get_all_with_criteria")
		return this.client.objects_get_all_with_criteria(params).then(result => {
			log.trace({
				type:typeObj.TYPE, 
				criteria, 
				results: result.map(i => { return _.pick(i, ['ObjectID', 'GUID'])})
			}, '#find returning results');

			return result.map(attrs => { return new typeObj(attrs) });
		});
	}


	/** Wrapper around find() which recursively calls find() until all 
	  results have been found. 

	  TODO: This is memory heavy. Revise to be more efficient.  Profile to see garbage collection performance
	  TODO: Change to stream interface
	*/
	findAll(typeObj, criteria, page=1, results=[]) {
		const limit = 1000;

		return this.find(typeObj, criteria, limit, page).then(res => {
			if (res.length < limit) {
				return results.concat(res);
			} else {
				page += 1;
				return this.findAll(typeObj, criteria, page, results.concat(res));
			}
		})
	}


	/** Find an object using multiple parallel searches with different criteria and
	 * return the first result based on priority.
	 * 
	 * @param {Ccure9kObject Subclass} type - The object type to find
	 * @param {Object} criteria - An object with name-value pairs used to search
	 * @param {Array[]} priority - An array of arrays, containing key combinations to search for
	 * @param {Object} scope - Additional criteria applied to every query without regard to priority
	 * @returns {Promise<Ccure9kObject>} the first matching object
	 * 
	 * @example
	 * //Search for Personnel using GUID, then EmailAddres, then both names
	 * findFirstMatch(Personnel, {
	 *    GUID:'12345-6789-12346789', 
	 *    EmailAddress:'test@example.com',
	 *    FirstName: 'John',
	 *    LastName: 'Smith'},
	 *    [['GUID'], ['EmailAddress'], ['Firstname', 'LastName']])
	 * .then(person => {
	 *    //output the first person found
	 *    console.log(person)
	 *  })
	 */
	findFirstMatch(type, criteria=[], priority, scope={}) {
		return this.findMultiple(type, criteria, priority, scope)
		.then(results => {
			return [].concat(...results)[0]; //Flatten array and return first element
		})
	}

	//TODO: work on nomenclature for #findFirstMatch() and #findMultiple(); 'priorities' should maybe be order, combinations? Ugh.
	/** Find objects using various combinations of search criteria in parallel and
	 * return the results based in order of priority given.
	 * 
	 * @param {Ccure9kObject Subclass} type - The object type to find
	 * @param {Object} criteria - An object with name-value pairs used to search
	 * @param {Array[]} priority - An array of arrays, containing key combinations to search for
	 * @param {Object} scope - Additional criteria applied to every query without regard to priority
	 * @returns {Array<Array>} All the result sets for all priorities given.
	 * 
	 * @example
	 * //Search for Personnel using GUID, then EmailAddres, then both names
	 * findFirstMatch(
	 *    Personnel,
	 *    {
	 *        GUID:'12345-6789-12346789', 
	 *        EmailAddress:'test@example.com',
	 *        FirstName: 'John',
	 *        LastName: 'Smith'
	 *    },
	 *    [['GUID'], ['EmailAddress'], ['Firstname', 'LastName']])
	 * .then(person => {
	 *    //output the first person found
	 *    console.log(person)
	 *  })
	 */
	findMultiple(type, criteria=[], priority, scope={}) {
		log.trace({type:type.TYPE, criteria, priority}, "#findMultiple()")
		
		return Promise.all(priority.map(i => {
			let combined_criteria = Criteria.and(_.pick(criteria, i), scope);

			return this.find(type, combined_criteria )
		}))
	}

	/**
	 * Find and return the first exact match.  If none is found, create it and
	 * return it.
	 */
	findOrCreate(type, criteria={}) {
		return Promise.all([
			this.find(type, criteria, 1, 1),
			this.findDefaultPartition()])
		.then(([results, defaultPartition]) => {
			log.debug({results, defaultPartition}, 'findOrCreate() initial results')
			if (results.length > 0) return results[0];
			else {
				log.debug("No results, creating new object");
				//Assign to default partition if not specified.
				if (!criteria.PartitionID) {
					criteria.PartitionID = defaultPartition.ObjectID;
				}
				return this.save(new type(criteria));
			}
		})
	}


	/** Destroy
	 * Deletes an object from CCURE
	 * @param {Ccure9kObject} obj - The object to destroy
	 * @returns {Promise<Ccure9kObject>} returns the deleted object
	 */
	destroy(obj) {
		if (!(obj instanceof Ccure9kObject)) throw new TypeError("Argument must be a Ccure9kObject!");

		return this.client.objects_delete(obj.Type, obj.ObjectID)
		.then(result => { 
			obj.deletedAt = new Date;
			return obj;
		})
	}

	
	findDefaultPartition() {
		if (this._default_partition) {
			return new Promise((resolve, reject) => {
				resolve(this._default_partition);
			});
		} else {
			return this.find(Partition, {IsDefaultPartition: true})
				.then(result => {
					this._default_partition = result[0];
					return this._default_partition;
				});
		}
	}


	get journal() {
		if (this._journal instanceof JournalPoller) return this._journal;
		return this._journal = new JournalPoller(this.client)
	}

}

module.exports = CcureService;