"use strict";

/* Credential Object */

const Ccure9kObject = require('./Ccure9kObject.js');
const moment = require('moment');
const _ = require('lodash');

const TYPE = 'SoftwareHouse.NextGen.Common.SecurityObjects.Credential'
const PROPERTIES = [
	"ObjectID",
	"GUID",
	"LastModifiedByID",
	"LastModifiedTime",
	"Protected",
	"Template",
	"AccessType",
	"Active",
	"Disabled",
	"Temporary", //Not a field in the schema and cannot be set, only read
	"Lost",
	"Stolen",
	"PersonnelID",
	"CardInt1",
	"CardInt2",
	"CardInt3",
	"CardInt4",
	"HMAC",
	"CHUID",
	"Name",
	"CHUIDFormatID",
	"ActivationDateTime",
	"ExpirationDateTime",
	"AgencyCode",
	"CredentialIssue",
	"CredentialSeries",
	"FacilityCode",
	"SystemCode",
	"IssueCode",
	"CardNumber",
	"PIN",
	"BadgePrintDate",
	"BadgeLayoutId",
	"FingerprintDateTime",
	"PartitionID",
	"SmartCardDateTime",
	"AssociationCategory",
	"OrganizationalCategory",
	"OrganizationalIdentifier",
	"PersonnelIdentifier",
	"DisabledByInactivity",
	"DisabledByInactivityDateTime",
	"ExpectedDate",
	"InactivityExempt",
	"DisabledByID",
	"Status",
	"IssueDate",
	"SmartID",
	"BLEEnabled",
	"AssignRandomCardNumber",
	'CardType',
	'HighAssuranceGUID',
	'FASCN',
	'TPK',
	'ChuidHash',
	'CakIssuer',
	'CakKey',
	'CakKeyType',
	'PakIssuer',
	'PakKey',
	'PakKeyType',
	'FingerprintImage',
	'Revoked',
	'RevokedReason',
	'OriginalCardID',
	'PacsId',
	'ClassType'
]
const FORMAT_DATETIME = "MM/DD/YYYY HH:mm:ss";


class Credential extends Ccure9kObject {

	constructor(obj) {
		super(TYPE, PROPERTIES, obj);

		//Fill in Defaults (and Required Attributes)
		_.defaults(this, {
			Name: `SV3 Visitor Badge ${this.CardNumber}`,
			CHUID: this.CardNumber,
			FacilityCode: 0,
			SmartID: "''", //SmartID must be empty string to pass CCURE validation (>= v.2.50)
			Temporary: true, //CCURE 2.70 does not have this as a writable field and will always default to trueS
			ActivationDateTime: moment().startOf('day').format(FORMAT_DATETIME),
			ExpirationDateTime: moment().endOf('day').format(FORMAT_DATETIME),
			Status: 0,
			CakIssuer: "''", //Required by CCURE 2.70
			PakIssuer: "''"  //Required by CCURE 2.70
		});
	}

	static get TYPE () { return TYPE }
	static get PROPERTIES () { return PROPERTIES }
	static get FORMAT_DATETIME () { return FORMAT_DATETIME }

}

module.exports = Credential;
