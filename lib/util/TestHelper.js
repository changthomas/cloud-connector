/**
 * TestHelper
 * 
 * Contains a number of useful utilities for managing test data in a CCURE test system
 *
**/

'use strict'

const Personnel = require('../ccure/Personnel.js');
const PersonnelType = require('../ccure/PersonnelType.js');
const Credential = require('../ccure/Credential.js');
const Group = require('../ccure/Group.js');
const GroupMember = require('../ccure/GroupMember');
const random = require('./random.js');


var nextCardNumber = 100000;

const personnelTypeMap = {};

//TODO: rename to DirectoryHelper -- This creates groups and such only useful for testing directory
class TestHelper {
	constructor(service) {
		this._service = service;
	}

	get service () {
		return this._service;
	}

	/**
	 * purge()
	 *
	 * Clears all Personnel, Personnel Groups, and any left-over Credentials
	 */
	purge() {
		return Promise.all([
			this.service.findAll(Personnel),
			this.service.findAll(Group, {GroupType: Personnel.TYPE})
		])
		.then(([personnel, groups]) => {
			console.log("TestHelper purging ALL Personnel")
			return Promise.all(
				personnel.map(p => { return this.service.destroy(p) })
			)
			.then(() => {
				console.log("TestHelper purging ALL Personnel Groups")
				return Promise.all(groups.map(g => { return this.service.destroy(g) }))
			})
		})
		/* Credential creation is disabled, no need to purge these records.
		.then(() => {
			//TODO: Scope Credentials by those without Personnel association (actual orphans)
			return this.service.findAll(Credential) //Grabs ALL credentials
		})
		.then(orphan_credentials => {
			return Promise.all(orphan_credentials.map(c => { return this.service.destroy(c) }))
		})
		.then(() => {
			nextCardNumber = 100000; //Reset card numbers
		})
		*/
	}

	/**
	 * populate()
	 *
	 * Populate the database with a sample set of 20,000 employee Personnel records.
	 * This will also generate a 100 Personnel Groups, and distribute the Personnel
	 * equally among them.
	 *
	 * It will then continue and create 1000 visitors.
	**/
	populate(employees=20000, groups=200) {
		const batchSize = (employees > 100) ? 100 : employees;
		
		console.log("Populating CCURE9000")

		return Promise.all(
			Array.from(Array(groups), (n, i) => {
				return this.service.save(new Group({
					Name: `Tenant ${i}`,
					GroupType: Personnel.TYPE
				}))
			})
		).then(async (groups) => {
			let done = 0;
			while (done < employees) {
				await Promise.all(Array.from(
					Array(batchSize), (n, i) => {
						return this.employee(nextCardNumber++, 
							groups[Math.floor(groups.length * Math.random())])
					}))
				done += batchSize;
				console.log("Completed %s", done);
			}
		})

	}

	/**
	 * employee()
	 *
	 * Generates a new employee with credential using the given cardNumber;
	**/
	employee(cardNumber, group) {
		return this.getType('Employee')
		.then(pType => {
			let p = new Personnel(new random());
			p.PersonnelTypeID = pType.ObjectID;
			return this.service.save( p );
		}).then(p => {
			return this.service.save( new GroupMember({
				GroupID: group.ObjectID,
				TargetObjectID: p.ObjectID,
				TargetObjectGUID: p.GUID,
				GroupType: Personnel.TYPE
			}))
		})
		//.then(p => {
		//	return this.service.save( new Credential({
		//		CardNumber: cardNumber, Name: `${p.Name} Card ${cardNumber}`}) )
		//})
	}


	getType(name) {
		return new Promise((resolve, reject) => {
			if (personnelTypeMap[name]) { 
				resolve(personnelTypeMap[name])
			} else {
				resolve(
					this.service.findOrCreate(PersonnelType, {Name: name})
					.then(t => {
						personnelTypeMap[name] = t;
						return t;
					})
				)
			}
		})
	}
}


/*
new Promise((resolve, reject) => {
	client.login((err, token) => {
		if (err) reject(err);
		else resolve(token);
	});
})
.then(() => {
	return service.find(PersonnelType, {Name: 'Employee'})
}).then(pType => {
	if (pType.length > 0) {
		return Promise.all(
			Array.from( new Array(300), (n,i) => {
				let p = new Personnel(new random());
				p.PersonnelTypeID = pType[0].ObjectID
				return service.save(p); 
			})
		);
	} else {
		throw new Error('Employee PersonnelType Not Found!');
	}
}).catch(err => {
	//Probably an uncaught ESOCKETTIMEDOUT due to overloading IIS.
	//TODO: Investigate further. Are we reaching the server's socket connection limit (probably)
	//or is this a Node.js limit?  Try rate limiting?
	console.log("Error!", err);
});
*/

module.exports = TestHelper;
