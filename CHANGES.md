# CHANGELOG

Please add entries here in reverse-chronological order (newest at the top)

# HEAD

* Added ./bin directory for executables
* Added ./bin/credential command for creating credential (troubleshooting customer issue)

# v2.4.4

* IMPROVEMENT: New configuration option to disable Visit creation.  When disabled, host matching is ignored and visit is not created.  This allows visitors to be added with credentials and clearances, without a host or visit


# v2.4.3

* BUGFIX: Scope visitor and host searches by PersonnelTypeID such that only the correct person is returned

# v2.4.2

* FEATURE: New Service.findAll() method returns all matches of a query by stitching together 1000 page results at a time.
* BUGFIX: Credential Export command used new CCURE Service, which was limited to 10 results. Now uses Service.findAll

# v2.4.1

1/18/2018 - Travis Sinnott

* FEATURE: Configuration parameter for Visitor Escort flag: ESCORTED or UNESCORTED
* BUGFIX: Old/depricated CCURE client library keepalive re-established; solves silent fetch_directory failure issue.
* UPDATE: Changed max Personnel request to 12000 in old CCURE lib.

# v2.4.0

1/16/2018 - Travis Sinnott (travis@buildingintelligence.com)

* FEATURE: Remote Exit command
* BUGFIX: Delete session-id and token from login requests

# v2.3.1

* FEATURE: Added support for disabling Visit creation
* UPDATE: Default logging settings now 1d period, 7 count, 1gb threshold, 5gb total


# v.2.3.0

1/5/2018 - Travis Sinnott (travis@buildingintelligence.com)

* FEATURE: Added fetch_credentials RPC command
* UPDATE: Updated LICENSE for 2018
* UPDATE: Updated author/contributor entries in package.json

## Release Notes

The new fetch_credentials RPC command is using the same basic (custom) RPC framework for simplicity.
The entire RPC mechanism will be replaced in a future version with a more mature framework like gRPC,
WAMP, or still custom RabbitMQ-based framework to be maintained as a separate project.


# v.2.2.0

* FEATURE: Added log rotation with gzip
* FEATURE: Added configuration options for log rotation
* FEATURE: Added console log option (from Vidsys CC codebase)
* BUGFIX: Check for clearances before trying to assign the same clearance(s)
* BUGFIX: KeepAlive uses client.schema_get() instead of client.version()
* IMPROVEMENT: Use USER_AGENT string for Web Service Client string to better identify connection
* IMPROVEMENT: Establish CCURE session before connecting to MQ


# v.2.0.0-RC1.SONY

7/28/2017 - Travis <travis@buildingintelligence.com>

## Release Notes

This release has not been fully tested and must not be released to all customers yet!  The focus of this new version is the addition of native C*CURE9000 Visitor features (>= v.2.50).  This required changes to Shortpath (to send host data), and many additions to and refactoring of the existing cloud connector.  Some functionality, namely the Journal/card reading feature required for Prox card assignment, was left untouched and also untested.  Given that there was no prior test coverage of these features, it is likely that something may be broken in this release.

__Therefore, it is recommended to only deploy this release specifically for the customer(s) that require the C*CURE9000 Visit feature and NOT the card reading feature (until fully tested).__

This release has also ONLY been tested with C*CURE9000 v.2.60 with Victor Web Services v.2.50 in a single-server configuration without any Partitions.  Behavior in an Enterprise (read: MAS/SAT) and/or Partitioned configuration may have unexpected behavior.  Based on past testing, if you aim the Cloud Connector at a single SAT node, default partitioning should apply and behave as expected. YMMV.

Details of the changes are enumerated below.  Note that there are BOTH process AND architecture changes.

## New Features

* Removed all licensing and configuration stuff.  This will be added again to a later version.
* __Installation now requires a manually crafted config.json file to be dropped in the install directory.__

#### Added support for C*CURE9000 (>= v2.50) Visitor and Visit objects

Upon visitor check-in in Shortpath:

1. A Personnel object for the visitor is created
	* NEW: The Personnel belongs to PersonnelType of 'SV3 Visitor', which is created if not found.
	* The Personnel is flagged as Escort Required (consistent with previous releases)
	* NEW: The Personnel.GUID matches the Shortpath Contact.uuid
2. The Personnel is assigned a Credential with the CardNumber given by Shortpath.  The Credential is only valid until 23:59 of the day issued, in local server time.
3. The Personnel is assigned the visitor credential(s)
4. NEW: A Visit object is created
	* NEW: The Host is associated to the Visit
	* NEW: The Visitor (Personnel) is associated to the Visit
	* NEW: The Visitor is "Checked-In"


#### Limitations

1. There is currently no check-out operation.  
2. Matching of Shortpath-to-CCURE identities relies on matching UUID/GUIDs.  Shortpath may not always be able to recognize that an individual is already in the system, and therefore a new Personnel/Visitor record will be created in BOTH systems.  This may be addressed later by requiring unique email addresses for visitors in Shortpath.


### New Project Architecture

* Created base classes for CCURE9000 objects
* Abstracted new web service client that closely matches the API
* Encapsulated business logic in new Service class
* Abstracted Shortpath AMQP and WebSocket clients into respective classes
	* Subclasses of EventEmitter! for fun and ease
* Abstracted KeepAlive behavior and now using API call to fetch version instead of logging in every 80 seconds.
* Client class now automatically retries request after logging in if initially met with Unauthorized response (if session has expired).
* Created test coverage for all new classes using Mocha and Chai.  Tests now reside directly beside their corresponding classes with the extension .spec.js.  At the time of writing, coverage is around 80% (of new code).
* Extensive use of ES6 and Promises make this code clean and easy to work with.
* JSDOC in-line documentation!!!!!  TODO: fix docs generation.


### Other (Developer) Notes

To future developers: GOMGZ! The CCURE Web Service API is so poorly designed it can be very frustrating to work with!  Part of the aim of creating the new Client and Service classes is to encapsulate the crazy and provide a nice clean ES6 framework for interfacing with the API.  This means use of classes, Promises, and other ES6 sugar.  Please continue this pattern and improve on it.

Note the data model classes created that extend from Ccure9kObject.  I could use some advise on how to handle the TYPE and PROPERTIES issue.  The goal is to have the classes define their own properties (or at least the ones we care about), and reference the Full Type Name of the CCURE Object Type.  This is used now for serialization prior to saving, and eventually for validation also.  How can this be better abstracted and managed at a superclass level?  Email me or (better yet) submit pull request.  Thanks!

ES2016+ is near, and Node v.8.x+ supports most of the spec already.  This means async/await functions! Which means way-cleaner Promise chaining!  This release is bundled with Node v.6.11.1 LTS.  Please target v.8.2+ for the next release and use ES2016+ only if the spec is released/published.
