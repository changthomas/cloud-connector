const nconf = require('./lib/config.js')
const log = require('./lib/logger.js')
const AMQPClient = require('./lib/sv3/AMQPClient.js')
const Cardholder = require('./lib/genetec/entity/Cardholder')
const CardholderGroup = require('./lib/genetec/entity/CardholderGroup')
const Visitor = require('./lib/genetec/entity/Visitor')
const LPRMonitor = require('./lib/genetec/lib/LPRMonitor')


class GenetecStarter {
  static start(){
   
    // get config
    let appCode = nconf.get('app:code')
    let mqConfig = nconf.get('mq')
    let pubsubOptions = {
      exchangeName: mqConfig.defaultExchangeName,
      subQueueName: `sv3-cc-${appCode}`
    }

    // connect mq
    let sv3Client = new AMQPClient()
    sv3Client.connect(mqConfig, pubsubOptions)

    // vehicle cc
    let lprEnabled = nconf.get('genetec:lprSDK:enabled')
    if(lprEnabled){
      // get config
      let lprDir = nconf.get('genetec:lprSDK:lprDir')
      let pubRouteKey = `cc-sv3vehicle-${appCode}`

      // watch dir      
      // this.watchLPRDir(lprDir, sv3VehicleClient)
      let lprMonitor = new LPRMonitor({dir: lprDir})
      lprMonitor.start()
      lprMonitor.on("lpr-read-detected", (json) => {
        sv3Client.send(json, pubRouteKey)
      })
    }

    // visitor cc
    let visitorEnabled = nconf.get('genetec:webSDK:enabled')
    if(visitorEnabled){
      //
      let pubRouteKey = 'cc-sv3visitor'

      //
      sv3Client.on('fetch_directory', function(json){
        log.debug("Received %s command. Trying...", json.action);
        try {
          Cardholder.getList().then((r) => {
            log.trace({data:r}, "fetchDirectory: returned (called success handler). Pushing to sv3Client")

            resJson = {
              personnels: [],
              groups: [],
              pairs: []
            }

            for(var i in r){
              let e = r[i]
              resJson.personnels.push({
                guid: e.guid,
                first_name: e.firstName,
                last_name: e.lastName,
                email: e.email
              })
            }

            sv3Client.send({
              customer_code: nconf.get('app:code'),
              data_type: "directory",
              data: resJson
            }, pubRouteKey)
          })
        } catch (err) {
          log.error({err: err}, "Failed to Fetch Directory!");
        }
      })


      //
      sv3Client.on('fetch_clearances', function(json){
        CardholderGroup.findVisitorGroups().then((groups) => {
            let groups_data = groups.map((g)=>{
              return {
                id: g.Guid[0],
                guid: g.Guid[0],
                name: g.Name[0]
              }
            })
            sv3Client.send({
              customer_code: nconf.get('app:code'),
              data_type: "clearances",
              data: groups_data
            }, pubRouteKey)
        })
      })

      //
      sv3Client.on('create_visitor', function(json){
        let visitorGroupName = nconf.get('genetec:visitor:groupName')
        Visitor.checkIn(json, visitorGroupName).then(r =>{
          log.info({visitor: json}, "Check-in completed")
        }).catch(err => {
          log.error({json, err}, "Error completing create_visitor command!")
        })
      })
    }
    
  }

  // static watchLPRDir(dir, sv3Client){
  //   fs.watch(dir, { encoding: 'buffer' }, (eventType, filename) => {
  //     if (filename) {
  //       let et = eventType.toString()
  //       if(et == 'change'){
  //         let fn = filename.toString()
  //         let fp = path.join(dir, fn)

  //         if(fs.existsSync(fp)){
  //           // console.log(`detected file ${fn}`)

  //           try{

  //             // make sure file write finished
  //             let stats = fs.statSync(fp)
  //             let fileSizeInBytes_old = stats.size
  //             let size_different =true
  //             while (size_different) { 
  //               size_different= !(fs.statSync(fp).size==fileSizeInBytes_old)
  //             }
              
  //             // parse xml
  //             let stream = fs.readFileSync(fp)
  //             let json = GenetecLPR.parseXML(stream)

  //             // console.log(json)
  //             sv3Client.send(json);
  //           }
  //           catch(err){
  //             console.log(err)
  //           }
  //           finally{
  //             try{
  //               fs.unlinkSync(fp)
  //             }
  //             catch(err){
  //               console.log("error delte file")
  //             }
  //           }
  //         }
          
  //       }
  //     }
  //   });
  // }

}

module.exports = GenetecStarter