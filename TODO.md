### TODOs
| Filename | line # | TODO
|:------|:------:|:------
| logger.js | 55 | Add file out method for debugging PowerShell and other IO operations
| ccure/Ccure9kObject.js | 16 | Add support for deep freeze (deep clone object and recursively freeze each property of type object)
| ccure/Ccure9kObject.js | 38 | return an object whose keys are properties that have changed, and the values are [before, after] tuples
| ccure/Clearance.spec.js | 28 | add reference to client
| ccure/Client.js | 63 | Make Login return Promise!
| ccure/Client.js | 151 | Refactor API into namespaced modules like Twilio node library
| ccure/Client.js | 195 | Client will encounter a 404 error when there are no results.  Fix this by returning [] instead.
| ccure/Client.js | 400 | reject() should return Error, not string
| ccure/Client.js | 507 | reject() should return Error, not string
| ccure/Client.spec.js | 63 | test for various error returns.  POST /Api/Objects/Persist can return: Error or non-200 status codes.
| ccure/Client.spec.js | 120 | update with use of new random();
| ccure/Credential.spec.js | 28 | add reference to client
| ccure/Criteria.js | 37 | Use rest/arguments to support any number of criteria sets
| ccure/Criteria.js | 38 | Complete JSDOC params and examples
| ccure/Personnel.js | 46 | inherit from Ccure9kObject
| ccure/Personnel.js | 64 | add Cards getter with WeakMap private data
| ccure/Personnel.js | 65 | override serialize() method to support Children.  See Visit for example.
| ccure/Personnel.spec.js | 51 | add reference to client
| ccure/PersonnelType.js | 32 | inherit from Ccure9kObject
| ccure/Service.js | 128 | This is memory heavy. Revise to be more efficient.  Profile to see garbage collection performance
| ccure/Service.js | 129 | Change to stream interface
| ccure/Service.js | 174 | work on nomenclature for #findFirstMatch() and #findMultiple(); 'priorities' should maybe be order, combinations? Ugh.
| ccure/Service.js | 318 | refactor find operation to return type expected.
| ccure/Service.js | 408 | Should PartitionID be explicitly specified here?
| ccure/Service.js | 416 | Handle non-unique email error; remove email attribute and try again.
| ccure/Service.js | 417 | Add support for visitor photo!
| ccure/Service.js | 458 | handle case where shortpathVisitObj is missing Visitor UUID and host information. (older versions)
| ccure/Service.js | 476 | Sanitize host matching!!!!
| ccure/Service.js | 496 | Don't delete card, just reassign it (when updating is supported)
| ccure/Service.spec.js | 336 | break this into multiple tests
| ccure/Service.spec.js | 388 | Implement tests for #assign that creates and assigns a credential
| ccure/Service.spec.js | 444 | uncomment this line
| ccure/Service.spec.js | 449 | complete this test.
| ccure/Service.spec.js | 555 | set config:ccure:visitor:match order explicitly here to:
| ccure/Service.spec.js | 573 | set config:ccure:visitor:match order explicitly here to:
| ccure/Service.spec.js | 869 | explicitly define config:ccure:visitor:match order to ensure Name priority
| ccure/Service.spec.js | 870 | explicitly define config:ccure:host:match order to ensure EmailAddress priority
| ccure/Service.spec.js | 913 | Complete this test. Currently causes CCURE errors
| ccure/Visit.spec.js | 48 | add reference to client
| ccure/Visit.spec.js | 108 | figure out why this gets called before results or people arrays are populated.
| sv3/AMQPClient.js | 108 | Move to callback, only called when command is completed and requeue if necessary.
| server.js | 53 | connect independently of CCURE login; hold requests until successful login
| server.js | 59 | begin polling card reads from Journal
| server.js | 143 | check to see if this is a temp badge for employee.  If yes, use old ccure.js library.
| server.js | 144 | implement temp badge issuance in new CcureService
| server.js | 145 | Message should be transactional: only confirm if checkin is successful!
| server.js | 354 | catch requests to shutdown and exit gracefully with a goodbye
| victor.js | 6 | Why is it necessary to recycle victor?  Under what conditions must this be done?